import { Meteor } from "meteor/meteor";
import { Contacts } from "../imports/api/contacts.js";
import "../imports/api/workshops.js";
import { Enrolments } from "../imports/api/enrolments.js";
import "../imports/api/users2.js";

Meteor.startup(() => {
  // process.env.MAIL_URL="smtps://daniel%40curiosocirco.com:fda94ab78368fe829ca1affe5e83e8587e427bdfbc4c49663147c544ae@peter.smtp-in.mailway.app:587/"
  process.env.MAIL_URL = `smtps://${encodeURIComponent(
    "daniel.skapunk"
  )}:${encodeURIComponent("opgfdoqgnuymctvo")}@smtp.gmail.com:465`;
});
//opgfdoqgnuymctvo

Router.route("/test/:secret", { where: "server" })

  // Get the auth info from header
  //secret = this.request.headers['x-user-id']
  //loginToken = this.request.headers['x-auth-token']
  // GET /message/:id - returns specific records
  .get(function () {
    if ("curiosopassword" != this.params.secret) {
      this.response.end("Error!\n");
      return;
    }
    //console.log(Meteor.user());
    var r = Contacts.find(
      {},
      { fields: { first_name: 1, last_name: 1, phone: 1 } }
    ).fetch();
    var response = CSV.unparse(r);
    this.response.setHeader("Content-Type", "application/json");
    this.response.end(JSON.stringify(response));
  });

Router.route("/health", { where: "server" }).get(function () {
  var cont = Contacts.find({ first_name: "Daniel" }).fetch();
  var enr = Enrolments.find({ year: 2020, month: 4 }).fetch();
  console.log("cont: ", cont);
  console.log("enr: ", enr);
  if (Contacts.rawDatabase().stats().ok) {
    this.response.statusCode = 200;
    this.response.end("ok");
  } else {
    this.response.statusCode = 500;
    this.response.end("db connection not ok" + cont);
  }
});

Meteor.publish(null, function () {
  if (this.userId) {
    var a = Meteor.roleAssignment.find({ "user._id": this.userId });
    return a;
  } else {
    this.ready();
  }
});

var createUsersAndRoles = function () {
  console.log("CREATE USERS AND ROLES");
  Meteor.users.remove({});
  Meteor.roles.remove({});
  Meteor.roleAssignment.remove({});
  Roles.createRole("admin", { unlessExists: true });
  Roles.createRole("teachers", { unlessExists: true });

  var id = Accounts.createUser({
    username: "daniel",
    email: "daniel@daniel.com",
    password: "daniel",
    emails: [
      {
        address: "daniel@daniel.com",
        verified: true,
      },
    ],
    profile: {
      first_name: "daniel",
      last_name: "daniel",
      contactId: "Kfixj5PCawywvKYtP",
    },
  });
  Roles.addUsersToRoles(id, ["admin"]);

  var id = Accounts.createUser({
    username: "vale",
    email: "vale@vale.com",
    password: "vale",
    emails: [
      {
        address: "vale@vale.com",
        verified: true,
      },
    ],
    profile: {
      first_name: "vale",
      last_name: "danel",
      contactId: "AzbZx4MkxC4DuLvrj",
    },
  });
  Roles.addUsersToRoles(id, ["teachers"]);
  var id = Accounts.createUser({
    username: "shwet",
    email: "shwet@shwet.com",
    password: "shwet",
    emails: [
      {
        address: "shwet@shwet.com",
        verified: true,
      },
    ],
    profile: {
      first_name: "shwet",
      last_name: "garg",
      contactId: "yZogcYvQ9ym4QpoLF",
    },
  });
  Roles.addUsersToRoles(id, ["teachers"]);
};

// createUsersAndRoles()
