FROM alpine:3.14
RUN apk add nodejs>14.0.0 npm
COPY ./build/curiosows.tar.gz /

##### ENV VARS ####
# ENV SERVER=curiosows.local
# ENV NAME=curiosows
ENV MONGO_URL="mongodb://curiosows:curiosows@curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017/curiosows?ssl=true&replicaSet=curioso-cluster-shard-0&authSource=admin&retryWrites=true&w=majority"
ENV PORT=3000
ENV ROOT_URL=http://127.0.0.1


RUN (mkdir -p /root/.npm/ && tar -xf /curiosows.tar.gz && cd /bundle/programs/server && npm install)
WORKDIR /bundle
CMD [ "node", "main.js" ]
