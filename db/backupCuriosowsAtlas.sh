#!/bin/bash
# Backups db in Mongo Atlas
# run it on the root of the project db/backupCuriosowsAtlas.sh
MONGO_DATABASE="curiosows"
MONGO_HOST="curioso-cluster-shard-0/curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017"
#MONGO_PORT="27017"
USERNAME=curiosows
PASSWORD=curiosows

NUM_BACKUPS=3

BACKUPS_DIR="db/db_backup/production"

TIMESTAMP=`date +%F-%H%M`
MONGODUMP_PATH="/usr/bin/mongodump"
BACKUP_NAME="$(date +'%Y%m%d%H%M')_${MONGO_DATABASE}_Atlas"
 
# mongo admin --eval "printjson(db.fsyncLock())"
# $MONGODUMP_PATH -h $MONGO_HOST:$MONGO_PORT -d $MONGO_DATABASE
$MONGODUMP_PATH -h $MONGO_HOST --ssl --authenticationDatabase admin -u $USERNAME -p $PASSWORD -d $MONGO_DATABASE -o $BACKUPS_DIR/$BACKUP_NAME
# mongo admin --eval "printjson(db.fsyncUnlock())"
 
#mkdir -p $BACKUPS_DIR
#mv dump $BACKUP_NAME
tar -zcvf $BACKUPS_DIR/$BACKUP_NAME.tgz $BACKUPS_DIR/$BACKUP_NAME
rm -rf $BACKUPS_DIR/$BACKUP_NAME

# Just leave N number of backup files tgz
cd $BACKUPS_DIR
ls -tp *.tgz | grep -v '/$' | tail -n +$((NUM_BACKUPS+1)) | xargs -I {} rm -- {}
