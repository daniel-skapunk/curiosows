/////////////////
execute DB script:

mongo --port 3001 bdscript.js

Run app with remota DB Mlab
MONGO_URL=mongodb://curiosows:curiosows@ds131320.mlab.com:31320/curiosows_mlab meteor

///////////////////
# LOCAL meteor DATABASE
///////////////////
## conect from mongo shell
```
mongo "mongodb://localhost:3001"
```

##RESTORE Local Database with Backup and drop existing collections
```
mongorestore -h 127.0.0.1 --port 3001 -d meteor --drop db_backup/20200326curiosows_mlab
```

##BACKUP local database
```mongodump --verbose -h=127.0.0.1:3001 --db=meteor -o=db_backup/$(date +'%Y%m%d%H%M')_curiosowslocal
```

///////////////////
# MLAB curiosows_mlab DATABASE
///////////////////
## MLAB connect address
mongodb://curiosows:curiosows@ds131320.mlab.com:31320/curiosows_mlab

##BACKUP REMOTE DATABASE
```
mongodump -h ds131320.mlab.com:31320 -d curiosows_mlab -u curiosows -p curiosows -o db_backup/$(date +'%Y%m%d%H%M')_curiosowsmlab
```

///////////////////
#ATLAS DATABASE curiosows
///////////////////
## connect address
```
mongodb+srv://curiosows:curiosows@curioso-cluster-eytmf.mongodb.net/curiosows?retryWrites=true&w=majority
```

## conect from mongo shell
```
mongo "mongodb+srv://curiosows:curiosows@curioso-cluster-eytmf.mongodb.net/curiosows?retryWrites=true&w=majority"

# for 2.2 < Node < 3.0
mongodb://curiosows:curiosows@curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017/curiosows?ssl=true&replicaSet=curioso-cluster-shard-0&authSource=admin&retryWrites=true&w=majority
```

##RESTORE REMOTE DATABASE
```
mongorestore --host curioso-cluster-shard-0/curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017 --ssl --username curiosows --password curiosows --authenticationDatabase admin -d curiosows db_backup/202005031212_curiosowsmlab/curiosows_mlab/
```

