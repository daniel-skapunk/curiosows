//RESTORE DB
mongorestore -h ds131320.mlab.com:31320 -d curiosows_mlab -u curiosows -p curiosows <input db directory> 

// BACKUP DB
mongodump -h ds131320.mlab.com:31320 -d curiosows_mlab -u curiosows -p curiosows -o db_backup

// RESTORE FROM DIRECTORY BACKUP TO LOCAL METEOR DEVELOPMENT DB
mongorestore -h 127.0.0.1 --port 3001 -d meteor db_backup/2017_09_20

// Connect to curiosows_mlab
mongo ds131320.mlab.com:31320/curiosows_mlab -u curiosows -p curiosows

// change parents from slug to id
db.category.find().forEach(function(doc) {
  doc.parent.forEach(function(v){
      r = db.category.findOne({slug:v});
      if(r){
          u1 = db.category.update({_id:doc._id},
            {
                $push:{parent:r._id}
            }
          );
          db.category.update({_id:doc._id},
            {
                $pull:{parent:v}
            }
          );
      }
  });
});

/////
// Change enrolments fee from string to double
/////
db.enrolments.find({}).forEach(function(doc) { 
    r = db.enrolments.update( 
       { _id: doc._id}, 
       { $set : { "fee" : parseInt(doc.fee) } }
    )
    print(r);
})

////////
/// Change enrolment.payments.amount to double
////////

db.enrolments.find({})
  .forEach(function (doc) {
    if(doc.payments){
        doc.payments.forEach(function (event) {
          if (event.amount) {
            event.amount=parseInt(event.amount);
          }
        });
        r = db.enrolments.save(doc);
        print(r);
    }
});

/////////
/// Get Stats sums of fee and payments
/////////
var month=3;
var year = 2017;
db.getCollection('enrolments').aggregate([
    { $match: { month: month, year:year } },
    { $unwind: "$payments" },
    {
        "$group": {
             "_id": "$_id",
             "workshopId": { $first: "$workshopId"},
             "fee": { $first: "$fee"},
             "amount": {
                 $sum: "$payments.amount"
             },
             "month": { $first: "$month"}
         }
    },
    {
        "$group": {
             "_id": "$workshopId",
             "workshopId": { $first: "$workshopId"},
             "fee": { $sum: "$fee"},
             "amount": {
                 $sum: "$amount"
             },
             "month": { $first: "$month"}
        }
    },
    {
        $lookup:{
          from: 'workshops',
          localField: 'workshopId',
          foreignField: '_id',
          as: 'workshop'
        }
    }
]);


/**
 * ADD attendances array to every Document that doesnt have the property
 */

db.getCollection('enrolments').updateMany({ attendances: {$exists:false} },
  {
    $set:{
      attendances:[],
    }
  }
)


/**
 * ADD ticketHistory array to every Document that doesnt have the property
 */

db.getCollection('enrolments').updateMany({ ticketHistory: {$exists:false} },
  {
    $set:{
      ticketHistory:[],
    }
  }
)

/**
 * ADD ticketSentStatus array to every Document that doesnt have the property
 */
db.getCollection('enrolments').updateMany({ ticketSentStatus: {$exists:false} },
  {
    $set:{
      ticketSentStatus:'NOT_SENT',
    }
  }
)

/**
 * ADD ticketSentStatus array to every Document that doesnt have the property
 */
db.getCollection('enrolments').find({ticketSent:{$exists:true}}).forEach(function(doc){
    var st='NOT_SENT';
    if(doc.ticketSent)
        st = 'SENT_UPDATED'
    db.getCollection('enrolments').update({_id:doc._id},{
        $set:{ticketSentStatus:st}
    });
    db.getCollection('enrolments').update({_id:doc._id},{
        $unset:{ticketSent:""}
    });
})
/////////////////
db.getCollection('enrolments').aggregate([
    {
        $match: {'month':11, 'year':2017}
    },
    { $unwind: {
            path: '$payments',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $group:
       {
               _id: {enrolmentId:"$_id", weekDay: "$attendances.weekDay", workshopId:"$workshopId", contactId:'$contactId'},
               paymentsSum: {$sum: "$payments.amount"},
               attendances: {$addToSet: "$attendances"}
       }
    },
    { $unwind: '$attendances' },
    {
            $lookup:
            {
               from: 'contacts',
               localField: '_id.contactId',
               foreignField: '_id',
               as: 'contact'
            }
    },
    { $unwind: '$contact' },
    {
            $group:
            {
               _id: {enrolmentId: "$_id.enrolmentId", weekDay: "$_id.weekDay", workshopId: "$_id.workshopId"},
               enrolments: { $push:  {contactId:'$_id.contactId', contact:'$contact', days: '$days', paymentsSum: '$paymentsSum'} }
            }
    },
     
])