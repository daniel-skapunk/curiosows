// run with: mongo --port 3001 db/snippets/bdscript.js
db = connect("127.0.0.1:3001/meteor");

// add days field to enrolments that doesn't have
var result = db.enrolments.updateMany( { 'days': null },
  [
    { $set: { "days": [] } }
  ]
);
printjson(result);

// // add days field to workshops that doesn't have
var result = db.workshops.updateMany( { 'days': null },
  [
    { $set: { "days": [] } }
  ]
);
printjson(result);

// adds one to days array to every workshop, except 6 that goes to 0
var result = db.workshops.updateMany( {},
  [
    { $set: { 
    	"days": {
          $map: {
             input: "$days",
             as: "day",
             in: {
             	$cond: { 
             		if: { $gte: [ "$$day", 6 ] }, then: 0, else: {$add: [ "$$day", 1 ]} 
             	} 
             }}
    	}
	  }}]
);
printjson(result);

// adds one to days array to every enrolment, except 6 that goes to 0
var result = db.enrolments.updateMany( {},
  [
    { $set: { 
    	"days": {
          $map: {
             input: "$days",
             as: "day",
             in: {
             	$cond: { 
             		if: { $gte: [ "$$day", 6 ] }, then: 0, else: {$add: [ "$$day", 1 ]} 
             	} 
             }}
    	}
	  }}]
);
printjson(result);

// adds one to days array to every enrolment.attendances, except 6 that goes to 0
var result = db.enrolments.updateMany( {},
  [
    { $set: { 
     "attendances": {
        $map: {
             input: "$attendances",
             as: "att",
             in: {
                $mergeObjects: ["$$att", { "weekDay": {
                    $cond: { 
                        if: { $gte: [ "$$att.weekDay", 6 ] }, then: 0, else: {$add: [ "$$att.weekDay", 1 ]} 
                    } 
                    //$add: ["$$att.weekDay",2] 
                } } ]
             }
        }
     }
    }}]
);

printjson(result);
