#!/bin/bash
MONGO_DATABASE="curiosows"
MONGO_HOST="curioso-cluster-shard-0/curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017"
USERNAME=curiosows
PASSWORD=curiosows

BACKUPS_DIR_PROD="db/db_backup/production"
BACKUPS_DIR_LOCAL="db/db_backup/local"
BACKUPS_DIR=""
BDBN_LOCAL="meteor"
BDBN_PROD="curiosows"
BDBN="" 

select opt in local production
do
  if [ $opt = local ]
		then
			BACKUPS_DIR=$BACKUPS_DIR_LOCAL
			BDBN=$BDBN_LOCAL
		else
			BACKUPS_DIR=$BACKUPS_DIR_PROD
			BDBN=$BDBN_PROD
	fi
  break
done

#mongorestore --host $MONGO_HOST --ssl -u $USERNAME -p $PASSWORD --authenticationDatabase admin -d $MONGO_DATABASE $MONGO_DATABASE/202005032104_dtrackermlab/dtracker_mlab/
cd $BACKUPS_DIR
options=(*.tgz)

PS3='Escoge Backup: '

select opt in "${options[@]}"
do
  v=${opt: 0 : -4}
  mkdir ./tmp
  tar -zxvf $opt -C ./tmp
  mongorestore --drop --host $MONGO_HOST --username $USERNAME --password $PASSWORD --ssl --authenticationDatabase admin -d $MONGO_DATABASE ./tmp/$BACKUPS_DIR/$v/$BDBN
  rm -fr ./tmp
  break;
done