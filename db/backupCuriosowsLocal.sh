#!/bin/bash
 
MONGO_DATABASE="meteor"
MONGO_HOST="127.0.0.1"
NUM_BACKUPS=3

BACKUPS_DIR="db/db_backup/local"

TIMESTAMP=`date +%F-%H%M`
MONGODUMP_PATH="/usr/bin/mongodump"
BACKUP_NAME="$(date +'%Y%m%d%H%M')_${MONGO_DATABASE}_Local"
 
# mongo admin --eval "printjson(db.fsyncLock())"
# $MONGODUMP_PATH -h $MONGO_HOST:$MONGO_PORT -d $MONGO_DATABASE
echo "$MONGODUMP_PATH -h $MONGO_HOST --port 3001 -d $MONGO_DATABASE -o $BACKUPS_DIR/$BACKUP_NAME"
$MONGODUMP_PATH -h $MONGO_HOST --port 3001 -d $MONGO_DATABASE -o $BACKUPS_DIR/$BACKUP_NAME
# mongo admin --eval "printjson(db.fsyncUnlock())"
 
#mkdir -p $BACKUPS_DIR
#mv dump $BACKUP_NAME
echo "tar -zcvf $BACKUPS_DIR/$BACKUP_NAME.tgz $BACKUPS_DIR/$BACKUP_NAME "
tar -zcvf $BACKUPS_DIR/$BACKUP_NAME.tgz $BACKUPS_DIR/$BACKUP_NAME 
rm -rf $BACKUPS_DIR/$BACKUP_NAME

# Just leave N number of backup files tgz
cd $BACKUPS_DIR
ls -tp *.tgz | grep -v '/$' | tail -n +$((NUM_BACKUPS+1)) | xargs -I {} rm -- {}
