#!/bin/bash

MONGO_DATABASE="meteor"
MONGO_HOST="127.0.0.1"

BACKUPS_DIR_PROD="db/db_backup/production"
BACKUPS_DIR_LOCAL="db/db_backup/local"
BACKUPS_DIR=""
BDBN_LOCAL="meteor"
BDBN_PROD="curiosows"
BDBN="" 

select opt in local production
do
  if [ $opt = local ]
		then
      echo LOCAL
			BACKUPS_DIR=$BACKUPS_DIR_LOCAL
			BDBN=$BDBN_LOCAL
		else
      echo PRODUCTION
			BACKUPS_DIR=$BACKUPS_DIR_PROD
			BDBN=$BDBN_PROD
	fi
  break
done
# exit
#mongorestore --host $MONGO_HOST --ssl -u $USERNAME -p $PASSWORD --authenticationDatabase admin -d $MONGO_DATABASE $MONGO_DATABASE/202005032104_dtrackermlab/dtracker_mlab/
cd $BACKUPS_DIR
options=(*.tgz)

PS3='Escoge Backup: '

select opt in "${options[@]}"
do
  v=${opt: 0 : -4}
  mkdir ./tmp
  tar -zxvf $opt -C ./tmp
  mongorestore --drop --host $MONGO_HOST --port 3001 -d $MONGO_DATABASE ./tmp/$BACKUPS_DIR/$v/$BDBN
  rm -fr ./tmp
  break;
done