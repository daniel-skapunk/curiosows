#!/bin/bash
SERVER=192.168.122.13
# SERVER=cnas.lan
PROJ_PATH=/home/daniel/cwssrc/build/bundle
# PROJ_PATH=/home/daniel/dev/all/curiosows/build/bundle
SUSER=daniel
NAME=curiosows
MONGO_URL="mongodb://curiosows:curiosows@curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017/curiosows?ssl=true&replicaSet=curioso-cluster-shard-0&authSource=admin&retryWrites=true&w=majority"
PORT=3000
ROOT_URL=http://127.0.0.1
UPLOAD=0
REMOTE=1
NVM_LOCAL=". /home/daniel/.nvm/nvm.sh && nvm use 14"
NVM_REMOTO=". /home/daniel/.nvm/nvm.sh && nvm use 14"


################################################################################
# Help                                                                         #
################################################################################
Help()
{
   # Display Help
   echo "script to build and deploy to nas"
   echo
   echo "Syntax: deploy [-b]"
   echo "options:"
   echo "b     if present build meteor app"
   echo "h     Print this Help."
   echo
}

while getopts ":buhl" option; do
  #  echo $option
   case $option in
      b) ./scripts/build-app.sh
         ;;
      u) UPLOAD=1
         ;;
      h) #display help
         Help
         exit;;
      l) REMOTE=0
         ;;
     \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
   esac
done

##################
# UPLOAD TO SERVER
##################
if [ $UPLOAD -eq 1 ]; then
   echo 'UPLOAD!!!';
   exit 1;
   echo -e "\n UPLOADING TO SERVER $SERVER \n"
   echo "rsync -avz ./build/bundle/ $SUSER@$SERVER:~/curiosows"
   rsync -avz ./build/bundle/ $SUSER@$SERVER:~/curiosows
fi


if [ $REMOTE -eq 1 ]; then
   echo 'REMOTE!!';
   exit 1;
   ########### NPM INSTALL AND PM2
   echo -e "\n NPM INSTALL on $SERVER \n"
   ssh $SUSER@$SERVER "$NVM && cd curiosows/programs/server/; npm install"
   ssh $SUSER@$SERVER "pm2 describe $NAME > /dev/null;"
   RUNNING=$?

   if [ "${RUNNING}" -eq 0 ]; then
   echo RESTARTING!
   ssh $SUSER@$SERVER "pm2 delete $NAME;"
   else
   echo STARTING
   # ssh $SUSER@curiosows.nas pm2 restart main
   fi 

   ######### START APP
   echo -e "\n STARTING APP on $SERVER \n"
   ssh $SUSER@${SERVER} "$NVM && export ROOT_URL=${ROOT_URL} &&\
   export PORT=${PORT} && export MONGO_URL=\"${MONGO_URL}\" &&\
   pm2 start --name ${NAME} ./curiosows/main.js"
   exit 0
fi

########### RUNNING ON LOCAL
   echo 'LOCAL!!!'
   # exit 1;
   echo 'Set nvm to use node 14'
   # nvm use 14;
   $NVM_LOCAL
   
   echo -e "\n NPM INSTALL on $SERVER \n"
   cd $PROJ_PATH/programs/server && npm install
   
   ## Check if another instance running
   pm2 describe $NAME > /dev/null;
   RUNNING=$?

   if [ "${RUNNING}" -eq 0 ]; then
      echo RESTARTING!
      pm2 delete $NAME;
   else
      echo STARTING
      # ssh $SUSER@curiosows.nas pm2 restart main
   fi 

   ######### START APP
   echo -e "\n STARTING APP on $SERVER \n"
   export ROOT_URL=${ROOT_URL} && export PORT=${PORT} && export MONGO_URL=${MONGO_URL} && pm2 start --name ${NAME} $PROJ_PATH/main.js
