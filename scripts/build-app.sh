#!/bin/bash
## run it with npm run build-app
#SERVER=curiosows.local
SERVER=127.0.0.1
NAME=curiosows
MONGO_URL="mongodb://curiosows:curiosows@curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017/curiosows?ssl=true&replicaSet=curioso-cluster-shard-0&authSource=admin&retryWrites=true&w=majority"
PORT=3000
ROOT_URL=http://127.0.0.1

echo -e "BUILDING METEOR APP \n"
#meteor build ./build --server-only --directory
#meteor build ../build --architecture os.linux.x86_64 --server-only
meteor build ./build --architecture os.linux.x86_64 --server-only --directory
