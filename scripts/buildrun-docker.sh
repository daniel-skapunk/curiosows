#!/bin/bash
docker build -t cws:1.0.0 .
docker run --name my-meteor-app -p 3000:3000 \
  --env ROOT_URL=http://127.0.0.1 \
  --env MONGO_URL="mongodb://curiosows:curiosows@curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017/curiosows?ssl=true&replicaSet=curioso-cluster-shard-0&authSource=admin&retryWrites=true&w=majority" \
  meteor-app
#docker build --build-arg EXACT_NODE_VERSION=false --build-arg NODE_VERSION=14.15.2 -t meteor-app .  
#docker run --env NODE_VERSION=14.15.2 --env EXACT_NODE_VERSION=false \
#  --env METEOR_PROFILE=1 --env METEOR_LOG=debug \
#  --env ROOT_URL=http://127.0.0.1 \
#  --env MONGO_URL="mongodb://curiosows:curiosows@curioso-cluster-shard-00-00-eytmf.mongodb.net:27017,curioso-cluster-shard-00-01-eytmf.mongodb.net:27017,curioso-cluster-shard-00-02-eytmf.mongodb.net:27017/curiosows?ssl=true&replicaSet=curioso-cluster-shard-0&authSource=admin&retryWrites=true&w=majority" \
#  --name my-meteor-app meteor-app