# Curiosows
Students and Workshops management system.

> **About**

> - 20 Jun 2016
> - Angularjs@1.5.7
> - angular-meteor@1.3.11

Pre reqs
- yarn
- meteor

## Run with Mongo Atlas DB
MONGO_URL='mongodb+srv://curiosows:curiosows@curioso-cluster.eytmf.mongodb.net/curiosows?retryWrites=true&w=majority' meteor

## Installation for development
1.- Clone the project:
```bash
git clone git@gitlab.com:daniel-skapunk/curiosows.git
```
2.- Installation
```bash
#cd path/to/my/project
#yarn
```
3.- Run Meteor Application
```bash
#meteor
```
4.- Add a user or import database
## add user as admin
(with meteor server started open another terminal and cd to the project)
```bash
meteor shell
# inside mongo shell
Accounts.createUser({username: 'test', password: 'test'})
# outputs: 'MkyKmeg78a5JK8tEh'
> Roles.addUsersToRoles('MkyKmeg78a5JK8tEh', ['admin']);
```
## import database
(with meteor server started open another terminal and cd to the project)
```bash
db/restoreCuriosowsLocal
```


5.- listo! Open localhost:3000

### Crear Usuarios y Roles
Para crear un usuario con roles, en server/main.js descomentar la funcion createUsersAndRoles, salvar, para que servidor meteor se reinicie y se ejecute la funcion una vez, despues de esto los roles y usuarios se habran creado

## Tips and More
- Run Meteor in another port:
```bash
meteor run --port 3030
```
- Slow compile/run? use profile at startup
```bash
METEOR_PROFILE=1 meteor
```
- Get local mongo url
``` bash
meteor mongo --url
# output: mongodb://127.0.0.1:3001/meteor
# connect with:
mongo 127.0.0.1:3001/meteor
```



# DEPLOY

## Deploy to Heroku
Install Heroku if  not installed yet
Linux:
```bash
sudo snap install --classic heroku
```
Add Remote branch to local repo 
```bash
heroku git:remote -a curiosows
```

When ready, just push to heroku remote branch to deploy

## Deploy to FreeBSD
```bash
npm run deploy-to-nas
```
see script/deploy.sh for reference


## HOW IS ORGANIZED
+ main.js establishes routes linking urls with templates (views)
+ each controller defines components and each components, in the definition of the component also a controller for the component is set