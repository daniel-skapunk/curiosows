import * as help from '../imports/helpers.js';
import _ from 'lodash';
import moment from 'moment';
moment.locale("es");

//// DEBUGGING
/// place debugger; instruction in a test
/// open chrome node inspector:
/// chrome://inspect/ --> dedicated Dev Tools for Node :9229
/// RUN
/// node --inspect-brk node_modules/.bin/jest --runInBand

test('test daysToString', ()=>{
    expect(help.daysToString()).toBe('');
    expect(help.daysToString([1,2])).toBe('Lunes Martes');
    expect(help.daysToString([1,2],3)).toBe('Lun Mar');    
});

test('test dayToString', ()=>{
    expect(help.dayToString(1,1)).toBe('L');
    expect(help.dayToString(1,2)).toBe('Lu');
    expect(help.dayToString(1,3)).toBe('Lun');
    expect(help.dayToString(1)).toBe('Lunes');    
});

test('test getNextOrPrevClosestInt', ()=>{
    // getNextOrPrevClosestInt(array, val, prev, same, cycle)
    expect(help.getNextOrPrevClosestInt([1,3], 0, true, false, true)).toBe(3);
    expect(help.getNextOrPrevClosestInt([1,3], 2, true)).toBe(1);
    expect(help.getNextOrPrevClosestInt([1,3], 2, false)).toBe(3);

    expect(help.getNextOrPrevClosestInt([1,3,5], 1, true)).toBe(null);
    expect(help.getNextOrPrevClosestInt([1,3,5], 1, true, true)).toBe(1);
    expect(help.getNextOrPrevClosestInt([1,3,5], 1, true, false, true)).toBe(5);
    
    expect(help.getNextOrPrevClosestInt([1,3,5], 1, false)).toBe(3);
    expect(help.getNextOrPrevClosestInt([1,3,5], 1, false, true)).toBe(1);
    expect(help.getNextOrPrevClosestInt([1,3,5], 5, false, false, true)).toBe(1);

    expect(help.getNextOrPrevClosestInt([1,3], 5, true)).toBe(3);
    expect(help.getNextOrPrevClosestInt([1,3], 5, false, false, true)).toBe(1); // what?
});

test('test findClosestDateInDaysArray', ()=>{
    // getNextOrPrevClosestInt(array, val, prev, same (false), cycle(true))
     
    // Lunes 2020 01 06 --> Lunes 2020 01 01
    expect(help.findClosestDateInDaysArray([1,3],moment('2020-01-06')).toDate())
        .toEqual(moment('2020-01-01').toDate());

    // Martes 2020 01 07 --> Lunes 2020 01 06    
    expect(help.findClosestDateInDaysArray([1,3],moment('2020-01-07')).toDate())
        .toEqual(moment('2020-01-06').toDate());

    // Miercoles 2020 01 08 --> Miercoles 2020 01 06
    expect(help.findClosestDateInDaysArray([1,3],moment('2020-01-08')).toDate())
        .toEqual(moment('2020-01-06').toDate());

    // Domingo 2020 01 05 --> Miercoles 2020 01 01
    expect(help.findClosestDateInDaysArray([1,3],moment('2020-01-05')).toDate())
        .toEqual(moment('2020-01-01').toDate());
});