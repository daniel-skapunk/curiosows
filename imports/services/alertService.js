'use strict';
// SERVICE alertService: 
angular.module('alertModule',[]).factory('alertService', ['$timeout', function($timeout) {
  var factory = {};

  factory.alerts = [];
  factory.secondaryAlerts = [];
  factory.loading = false;

  factory.alert = function(_alert){
    //factory.alerts.push(_alert);
    // factory.alerts.length = 0;
    console.debug('alert!!!');
    factory.alerts.push(_alert);
    $timeout(factory.closeAlert, 7000);
  };
  factory.secondaryAlert = function(_alert){
    factory.secondaryAlerts.length = 0;
    factory.secondaryAlerts.push(_alert);
    $timeout(factory.closeSecondaryAlert, 7000);
  };
  
  factory.error = function(message) {
    factory.alert({type:'danger', message: message});
  };
  factory.success = function(message) {
    factory.alert({type:'success', message: message});
  };

  factory.closeAlert = function(index) {
    factory.alerts.splice(index, 1);
  };
  factory.closeSecondaryAlert = function(index) {
    factory.secondaryAlerts.splice(index, 1);
  };

  factory.getAlerts = function(){
    return factory.alerts;
  };
  factory.getSecondaryAlerts = function(){
    return factory.secondaryAlerts;
  };

  return factory;
}]);