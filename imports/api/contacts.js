import SimpleSchema from 'simpl-schema'; 
import { Mongo } from 'meteor/mongo';
import { Enrolments } from '../api/enrolments.js';
import { Counts } from 'meteor/tmeasday:publish-counts';
 
export const Contacts = new Mongo.Collection('contacts');

ContactsSchema = new SimpleSchema({
  first_name: {
    type: String,
    label: "Contact first name"
  },
  last_name: {
    type: String,
    label: "Contact last name"
  },
  phone: {
    type: String,
    label: 'phone number'
  },
  email: {
    type: String,
    label: 'Contact email',
    regEx: SimpleSchema.RegEx.Email
  },
  facebook:{
    type: String,
    label: 'Contact facebook'
  },
  created: {
    type: Date,
    label: 'creation Date'
  },
  lastEdition: {
    type: Date,
    label: 'Contact last edition Date'
  },
  responsiblePerson: {
    type: String,
    label: 'Responsible Person contact information',
    defaultValue: '',
    optional: true
  },
  responsabilityLetter: {
    type: Boolean,
    label: 'If the Contact signed responsabilityLetter',
    defaultValue: false,
    optional: true
  },
});

Contacts.attachSchema(ContactsSchema);

if(Meteor.isServer){
   // This code only runs on the server
  Meteor.publish('contacts', function (options, filter) {
    if(!this.userId)
      return this.ready();
    // console.log(options);
    // console.log(filter);
    if(!filter) filter = {search:''};
    Counts.publish(this, 'numberOfContacts', Contacts.find(), {
      noReady: true
    });
    return Contacts.find({$or: [
      { 'first_name': {'$regex': filter.search, $options: 'i'} }
    ]},options);
  });
}

Contacts.getFullName = function(contact){
  return contact.first_name + ' ' + contact.last_name;
};

Meteor.methods({
  'contacts.insertupdate' (newContact) {
    // TODO: Make sure the user is logged in before inserting a task
    // if (!Meteor.userId()) {
    //   throw new Meteor.Error('not-authorized');
    // }
    var contact = {
      first_name: newContact.first_name,
      last_name: newContact.last_name,
      phone: newContact.phone,
      email: newContact.email,
      facebook: newContact.facebook,
      created: new Date(),
      responsiblePerson: newContact.responsiblePerson,
      responsabilityLetter: newContact.responsabilityLetter
    };
    
    if(newContact._id){
      r = Contacts.update({_id:newContact._id}, {$set: contact});
    }else{
      contact.lastEdition= new Date();
      r = Contacts.insert(contact);
    }
    console.log(r);
  },
  // insert update captcha check
  'contacts.captchainsert' (newContact, response) {
    console.log('newContact',newContact, response)
    HTTP.call('POST', 'https://www.google.com/recaptcha/api/siteverify', {
      data: {
        secret:'6LfdvSAaAAAAAL_tfdbffMb8vzUAgQwyle-Q1Bm_',
        response :response
      }
    }, (error, result) => {
      console.log('error-result', error, result)
      if (!error) {
        // Session.set('twizzled', true);
      }
    });
    // console.log(r);
  },
  'contacts.update' (newContact) {
    Contacts.update(newContact._id, newContact);
  },
  'contacts.remove' (contact) {
    var contactId = contact._id;
    Contacts.remove(contactId);
  },
  'contacts.find' (id) {
    var c =  Contacts.findOne({_id:id});
    // console.log(c);
    return c;
  },
});

Contacts.after.remove(function (contactId, doc) {
  console.log('after remove');
  Enrolments.remove({contactId: doc._id});
});