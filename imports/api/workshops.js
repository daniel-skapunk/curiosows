import { Mongo } from 'meteor/mongo';
import { Contacts } from '../api/contacts.js';
import * as help from '../helpers.js'
 
export const Workshops = new Mongo.Collection('workshops',{
  transform: function(doc) {
    //console.log(doc._id);
    if (!doc._id){
      //console.log('no doc._id');
      //console.log(doc);
      return doc;
    }
    //console.log('Workshops: doc._id',doc._id);
    doc.teacher = Contacts.findOne({_id: doc.teacherId});
    //console.log(doc.teacher);
    return doc;
  }
}); 

if(Meteor.isServer){
   // This code only runs on the server
  Meteor.publish('workshops', function workshopsPublication() {
    console.log('Meteor.userId(): ', Meteor.userId());
    var uid = Meteor.userId();
    //var res = Roles.userIsInRole(uid, 'teachers', 'curiosows');
    if(!this.userId)
      return this.ready();
    var workshops = Workshops.find();
    var contactIds = workshops.map(function(e) { 
      return e.teacherId 
    });
    var contacts = Contacts.find({_id: {$in: contactIds} });
    //console.log(contacts.fetch());
    r = [
      workshops,
      contacts
    ];
    return r;
  });
}

Meteor.methods({
  'workshops.insert' (newWorkshop) {
    //check(text, String);
 
    // Make sure the user is logged in before inserting a task
    // if (!Meteor.userId()) {
    //   throw new Meteor.Error('not-authorized');
    // }
    
    Workshops.insert({
      name: newWorkshop.name,
      teacherId: newWorkshop.teacherId,
      days: newWorkshop.days,
      created: new Date(),
      archived: false,
      // owner: Meteor.userId(),
      // username: Meteor.user().username,
    });
  },
  'workshops.update' (newWorkshop) {
    Workshops.update(newWorkshop._id, newWorkshop);
  },
  'workshops.softDelete' (workshop) {
    var workshopId = workshop._id;
    Workshops.update(workshopId, { $set: { archived: true} });
    //Workshops.update(workshopId);
  },
  'workshops.restore' (workshop) {
    var workshopId = workshop._id;
    Workshops.update(workshopId, { $set: { archived: false} });
    //Workshops.update(workshopId);
  },
  'workshops.remove' (workshop) {
    var workshopId = workshop._id;
    Workshops.remove(workshopId);
  },
});

///////////
/// Workshops helper functions
/////////////

export var WorkshopsHelpers = {
  workshopToString: function(workshop,nDays=null){
    //console.log('workshop: ', workshop);
    var r = (workshop.name + ' - ' + help.daysToString(workshop.days, nDays));
    return r;
  }
}