//import SimpleSchema from 'simpl-schema'; 
//import { Mongo } from 'meteor/mongo';
//import { Enrolments } from '../api/enrolments.js';
//import { Counts } from 'meteor/tmeasday:publish-counts';
 
//export const Users = new Mongo.Collection('users');

// ContactsSchema = new SimpleSchema({
//   first_name: {
//     type: String,
//     label: "Contact first name"
//   },
//   last_name: {
//     type: String,
//     label: "Contact last name"
//   },
//   phone: {
//     type: String,
//     label: 'phone number'
//   },
//   email: {
//     type: String,
//     label: 'Contact email',
//     regEx: SimpleSchema.RegEx.Email
//   },
//   facebook:{
//     type: String,
//     label: 'Contact facebook'
//   },
//   created: {
//     type: Date,
//     label: 'creation Date'
//   },
//   lastEdition: {
//     type: Date,
//     label: 'Contact last edition Date'
//   },
//   responsiblePerson: {
//     type: String,
//     label: 'Responsible Person contact information',
//     defaultValue: '',
//     optional: true
//   },
//   responsabilityLetter: {
//     type: Boolean,
//     label: 'If the Contact signed responsabilityLetter',
//     defaultValue: false,
//     optional: true
//   },
// });

// Contacts.attachSchema(ContactsSchema);

if(Meteor.isServer){
  // This code only runs on the server
  
  // Publish users... if users changes meteor send data to clients
  Meteor.publish('usersList', function (options) {
    console.log('users published');
    if(!this.userId)
      return this.ready();
    return Meteor.users.find({},options);
  });
}

// Contacts.getFullName = function(contact){
//   return contact.first_name + ' ' + contact.last_name;
// };

Meteor.methods({
  'users.add' (newUser) {
    console.log('user insert: ', newUser); 
    // TODO: Make sure the user is logged in before inserting a task
    // if (!Meteor.userId()) {
    //   throw new Meteor.Error('not-authorized');
    // }
    Accounts.createUser(newUser);
    return("OK");
  },
  'users.find' () {
    console.log('user find: ');
    //var roles Meteor.roleAssignment.find();
    // TODO: Make sure the user is logged in before inserting a task
    // if (!Meteor.userId()) {
    //   throw new Meteor.Error('not-authorized');
    // }
    var users =  Meteor.users.find({},{ fields: {username:1, emails:1, profile:1} } ).fetch();
    users.forEach( function(user, index) {
      console.log('user.id: ', user._id);
      var roles = Roles.getRolesForUser(user._id);
      console.log('roles: ', roles);
      user.roles = roles;
    });
    return users;
  },
  'users.saveRoles' (users) {
    users.forEach( function(user, index) {
      var userid =  user._id;
      var role = user.roles.length >0 ? user.roles[0] : '';
      Roles.removeUsersFromRoles(userid,['admin','teachers']);
      Roles.setUserRoles( userid, role );
      //var roles = Roles.getRolesForUser(user._id);
      //console.log('roles: ', roles);
      //user.roles = roles;
    });
  }
});