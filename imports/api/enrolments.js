import SimpleSchema from 'simpl-schema'; 
import { Mongo } from 'meteor/mongo';
import { Workshops, WorkshopsHelpers } from '../api/workshops.js';
import { Contacts } from '../api/contacts.js';

import * as helpers from '../helpers.js';

export const Enrolments = new Mongo.Collection('enrolments', {
	transform: function(doc) {
		//console.log(doc._id);
		if (!doc._id){
			//console.log('no doc._id');
			//console.log(doc);
			return doc;
		}
		doc.workshop = Workshops.findOne({_id: doc.workshopId});
		doc.contact = Contacts.findOne({_id: doc.contactId});
		// console.log(Contacts.findOne({_id: doc.contactId}));
		// console.log("doc._id: " + doc._id);
		// console.log("doc.contact: " + doc.contact._id);

		var paymentsSum = 0;
		doc.paymentsSum = paymentsSum;
		doc.isPaid = false;
		if(doc.payments){
			for (var i = 0; i < doc.payments.length; i++) {
				paymentsSum += doc.payments[i].amount*1;
			}
			doc.paymentsSum = paymentsSum;
			if(doc.paymentsSum >= doc.fee){
				doc.isPaid = true;
			}
		}
		return doc;
	}
});

EnrolmentsSchema = new SimpleSchema({
	contactId: {
		type: String,
		label: "Contact Id"
	},
	workshopId: {
		type: String,
		label: "WorkshopId"
	},
	month: {
		type: SimpleSchema.Integer,
		label: 'enrolment month'
	},
	year: {
		type: SimpleSchema.Integer,
		label: 'enrolment year'	
	},
	oneClass: {
		type: Boolean,
		label: 'true if its a one class enrolment'
	},
	oneClassDate: {
		type: Date,
		label: 'One Class Date',
		optional: true
	},
	// sampleClass:{
	// 	type: Boolean,
	// 	label: 'true if its a sample class enrolment'
	// },
	fee: {
		type: Number,
		label: 'enrolment agreed fee'
	},
	payments: {
		type: Array,
		label: 'payments'
	},
	'payments.$': Object,
	'payments.$.paymentDate': Date,
	'payments.$.amount': Number,
	'payments.$.opened': {
		type:Boolean,
		optional: true
	},
	days: [SimpleSchema.Integer],
	note: {
		type: String,
		label: "Note",
		optional: true
	},
	ticketSentStatus: {
		type: String,
		defaultValue: 'NOT_SENT',
		label: 'NOT_SENT: email not sent yet; SENT_UPDATED: Sent and no changes have been made; SENT_OUTDATED',
		optional: true
	},
	
	ticketHistory: {
		type: Array,
		label: 'list of sent tickets and info'
	},
	'ticketHistory.$': Object,
	'ticketHistory.$.sentDate': Date,
	'ticketHistory.$.workshopName': String,
	'ticketHistory.$.contactName': String,
	'ticketHistory.$.workshopMonth': String,
	'ticketHistory.$.workshopYear': Number,
	'ticketHistory.$.enrolmentFee': Number,
	'ticketHistory.$.enrolmentPaymentsSum': Number,
	'ticketHistory.$.numberOfClasses':Number, 

	attendances:{
		type: Array,
		label: 'attendance array'
	},
	'attendances.$': Object,
	'attendances.$.weekDay': Number,	// 0 to 6 values represent week day
	'attendances.$.day': 	Number,		// number of the day
	'attendances.$.status': Number,		// -1: No, 0: Not set, 1: Yes

	created: {
		type: Date,
		label: 'Enrolment creation Date'
	},
	lastEdition: {
		type: Date,
		label: 'Enrolment last edition Date'
	},			
});

Enrolments.attachSchema(EnrolmentsSchema);

/**
 * Checks if enrolment is Paid based on payments and fee
 * @param  {Object}  enrolment 
 * @return {Boolean}           true if paid, else if not
 */
Enrolments.isPaid = function(enrolment){
	result = false;
	var sum = _.reduce(enrolment.payments, function(memo, num){ 
		return memo + num.amount;
	}, 0);
	if(sum>=enrolment.fee)
		result = true;
	return result;
};


if(Meteor.isServer){
	 // This code only runs on the server
	Meteor.publish('enrolmentsAll', function(month, year) {
		//console.log(month);
		if(!this.userId)
			return this.ready();
		var enrolments = Enrolments.find();
		var workshopIds = enrolments.map(function(e) { return e.workshopId });
		var contactIds = enrolments.map(function(e) { return e.contactId });
		var workshops = Workshops.find({_id: {$in: workshopIds} });
		var contacts = Contacts.find({_id: {$in: contactIds} });
		//console.log(contacts.fetch());
		r = [
		enrolments,
		workshops,
		contacts
		];

		return r;
	 });

	 Meteor.publish('enrolments', function(month, year){
		var srchObj = {};
		if(month){ srchObj.month = month; }
		if(year){ srchObj.year = year; }
		return Enrolments.find(srchObj);
	 });

	 Meteor.publish('enrolments_workshops', function(workshopIds, contactIds){
		//console.log(workshopIds);
		workshops =  Workshops.find({_id: {$in: workshopIds}});
		contacts = Contacts.find({_id: {$in: contactIds} })
		return [workshops, contacts];
	});



	Meteor.methods({
		'enrolments.insert' (newEnrolment) {
			//console.log('enrolments.insert');
			//check(text, String);

			// Make sure the user is logged in before inserting a task
			// if (!Meteor.userId()) {
			//   throw new Meteor.Error('not-authorized');
			// }
			// var payment = {
			//   paymentDate:new Date(),
			//   amount: newEnrolment.payment
			// };
			//console.log(newEnrolment.attendances);

			
			Enrolments.insert({
				contactId: newEnrolment.contactId,
				workshopId: newEnrolment.workshopId,
				month: newEnrolment.month,
				year: newEnrolment.year,
				oneClass: newEnrolment.oneClass,
				oneClassDate: newEnrolment.oneClassDate,
				fee: newEnrolment.fee,
				payments:newEnrolment.payments,
				days: newEnrolment.days,
				note: newEnrolment.note,
				attendances: newEnrolment.attendances,
				lastEdition: new Date,
				created: new Date(),
				ticketHistory: [],
				ticketSentStatus: 'NOT_SENT'
			});
		},

		'enrolments.addPayment' (enrolment) {
			console.log('addPayment');
			var ticketSentStatus = enrolment.ticketSentStatus=='SENT_UPDATED'? 'SENT_OUTDATED':enrolment.ticketSentStatus;
			var r = Enrolments.update(enrolment._id,
				{$set: {
					'payments': enrolment.payments,
					'ticketSentStatus': ticketSentStatus
				}}); 
			//console.log(r);
		},

		'enrolments.updateAttendances' (enrolmentId, day, status) {
			//console.log('enrolments.updateAttendances');
			
			var enr = Enrolments.findOne({_id:enrolmentId});
			var att = enr.attendances;
			
			i = _.findIndex(att, {day:parseInt(day)});
			//console.log('index');
			//console.log(i);
			att[i].status = status;

			var r = Enrolments.update(enrolmentId,
				{$set: {'attendances': att}}
			); 
		},
		
		'enrolments.count' (month, year, workshopId) {
			console.log('enrolments.count');
			//var enrolments = Enrolments.find({month: month, year:year, workshopId:workshopId});
			var pipeline=[
			{ $match: { month: month, year:year } },
			{ 
				$unwind: 
				{
					path: "$payments",
					preserveNullAndEmptyArrays: true
				}
			},
			{
				"$group": {
					"_id": "$_id",
					"workshopId": { $first: "$workshopId"},
					"fee": { $first: "$fee"},
					"amount": {
						$sum: "$payments.amount"
					},
					"month": { $first: "$month"}
				}
			},
			{
				"$group": {
					"_id": "$workshopId",
					"workshopId": { $first: "$workshopId"},
					"income": { $sum: "$fee"},
					"actualIncome": {
						$sum: "$amount"
					},
					"month": { $first: "$month"}
				}
			},
			{
				$lookup:{
					from: 'workshops',
					localField: 'workshopId',
					foreignField: '_id',
					as: 'workshop'
				}
			}
			];
			var data = Enrolments.aggregate(pipeline);
			return data;
		},
		'enrolments.update' (newEnrolment) {
				console.log('enrolments.update');
				var ticketSentStatus = newEnrolment.ticketSentStatus=='SENT_UPDATED'? 'SENT_OUTDATED':newEnrolment.ticketSentStatus;
				var r = Enrolments.update(
					{_id:newEnrolment._id},
					{
						$set: {
							'contactId': newEnrolment.contactId,
							'workshopId': newEnrolment.workshopId,
							'month': newEnrolment.month,
							'year': newEnrolment.year,
							'oneClass': newEnrolment.oneClass,
							'oneClassDate': newEnrolment.oneClassDate,
							'fee': newEnrolment.fee,
							'payments': newEnrolment.payments,
							'days': newEnrolment.days,
							'ticketSentStatus': ticketSentStatus,
							'note': newEnrolment.note,
							'attendances': newEnrolment.attendances,
							'lastEdition': new Date()
						}
					}
				);
			//Enrolments.update({_id:newEnrolment._id}, newEnrolment);
		},
		
		'enrolments.insertupdate' (newEnrolment) {
			console.log('insertupdate');
			var enr = {
				'contactId': newEnrolment.contactId,
				'workshopId': newEnrolment.workshopId,
				'month': newEnrolment.month,
				'year': newEnrolment.year,
				'oneClass': newEnrolment.oneClass,
				'oneClassDate': newEnrolment.oneClassDate,
				'fee': newEnrolment.fee,
				'payments':newEnrolment.payments,
				'days': newEnrolment.days,
				'note': newEnrolment.note,
				'attendances': newEnrolment.attendances,
				'lastEdition': new Date(),
				'ticketSentStatus':newEnrolment.ticketSentStatus
			}
			// Insert Case
			if(!newEnrolment._id){
				enr._id=newEnrolment._id;
				enr.ticketSentStatus= 'NOT_SENT';
				enr.created= new Date();
				enr.ticketHistory= [];
			}
			// UPDATE CASE
			else{
				console.log('update Case');
				// if newEnrolment has ticketSentStatus, user is manually changing status
				if(newEnrolment.ticketSentStatus){
					console.log('newEnrolment.ticketSentStatus');
					enr.ticketSentStatus = newEnrolment.ticketSentStatus;
				}
				else{ // change ticket Status automatically
					var e = Enrolments.findOne({_id:newEnrolment._id});
					if(e.ticketSentStatus=='SENT_UPDATED'){
						// check if relevant properties changed
						// var changed = helpers.checkIfChanged(e, newEnrolment);
						var changed = checkIfChanged(e, newEnrolment);
						if(changed)
							enr.ticketSentStatus = 'SENT_OUTDATED';
					}
				}
			}
			if(newEnrolment._id)
				Enrolments.update({_id:newEnrolment._id}, {$set:enr});
			else
				Enrolments.insert(enr);
		},

		'enrolments.remove' (enrolment) {
			var enrolmentId = enrolment._id;
			Enrolments.remove(enrolmentId);
		},
		'enrolments.find' (id) {
			var c =  Enrolments.findOne({_id:id});
			//console.log(c);
			return c;
		},

		'enrolments.isAlreadyEnrolled' (contactId, workshopId, year, month) {
			var enr = Enrolments.find({contactId: contactId, workshopId: workshopId, year: year, month:month});
			if(enr.length>0) return enr;
			else return false;
		},

		'enrolments.insertUpdateIfExists' (enrolment) {
			var enrToSave = enrolment;
			var enr = Enrolments.findOne({ contactId: enrolment.contactId, 
				workshopId: enrolment.workshopId,
				year: enrolment.year, month:enrolment.month});
			//console.log("ENROLMENT:", enr);
			if(enr){
				var attendances = helpers.mergeAttendances(enr.attendances, enrolment.attendances);
				enr.attendances = attendances;
				var days = helpers.mergeDays(enr.days,enrolment.days);
				enrToSave = enr;
				enrToSave.days = days;
			}

			console.log('ENROLMENT',enrToSave);
			
			if(enrToSave.hasOwnProperty("_id") ) 
				Enrolments.update({_id:enrToSave._id}, {$set:enrToSave});
			else{
				enrToSave.ticketHistory = [];
				enrToSave.created= new Date();
				enrToSave.lastEdition= new Date();
				Enrolments.insert(enrToSave);
			}
		},
		
		'enrolments.sendTicket' (to, from, subject, enrolment) {
			// Make sure that all arguments are strings.
			//check([to, from, subject, text], [String]);
			// Let other method calls from the same client start running, without
			// waiting for the email sending to complete.
			
			SSR.compileTemplate('ticketTemplate', Assets.getText('ticketTemplate.html'));

			// console.log(enrolment);
			data = enrolment;

			var htmlBody = SSR.render('ticketTemplate', data);

			// console.log(htmlBody);

			this.unblock();
			Email.send({to: to, from: from, bcc:'contacto@curiosocirco.com', subject: subject, html: htmlBody });
			
			// update enrolment email sent
			var ticketHistoryObj = {
				sentDate: new Date(),
				workshopName: data.workshop.name,
				contactName: data.contact.first_name + ' ' + data.contact.last_name,
				workshopMonth: data.monthString,
				workshopYear: data.year,
				enrolmentFee: data.fee,
				enrolmentPaymentsSum: data.paymentsSum,
				numberOfClasses: data.numberOfClasses
			};

			var r = Enrolments.update({_id:enrolment._id},
				{	$set: { 
						'ticketSentStatus': 'SENT_UPDATED',
					},
					$addToSet: {
						'ticketHistory': ticketHistoryObj
					}
				}
			);
			//console.log(r);
			//console.log('todo biem!');
			

			// Meteor.defer( function(){
		 //    	Email.send({to: to, from: from, bcc:'contacto@curiosocirco.com', subject: subject, html: htmlBody });
		 //    	// update enrolment email sent
		 //    	console.log('todo biem!');
			// });
		},

		'enrolments.findByWeekDay' (month, year, oneClass) {
			if(!this.userId)
				return [];
			//console.log(month);
			// console.log(year);
			year = year*1;
			enrolments = {};

			for (var weekDay = 0; weekDay < 7; weekDay++) {
				var workshopsCursor = Workshops.find({days:weekDay, archived: {$ne: true}},{ fields: {_id:1, name:1} });
				//console.log(workshopsCursor.fetch());
				enrolments[weekDay] = {};
				enrolments[weekDay].workshops = workshopsCursor.fetch();
				workshops = enrolments[weekDay].workshops;
				for (var i = 0; i < workshops.length; i++) {
					workshop = workshops[i];
					var options = {
						workshopId:workshop._id,
						year:year,
						month:month,
						//month:'Agosto',
						//$and: [ {oneClass:{ $exists:oneClass} } , {oneClass:oneClass} ]
					};
					if(oneClass){
						options.$and = [ {oneClass:{ $exists:oneClass} } , {oneClass:oneClass}, {days:weekDay} ];
						options.days = weekDay;
					}else{
						options.$or = [ {oneClass:null } , {oneClass:oneClass}, {days:weekDay} ];
					}
					//console.log(options.$or);
					enrolmentsCursor = Enrolments.find(options);
					workshop.enrolments = enrolmentsCursor.fetch();
					//console.log(enr);
				}
			}
			
			// var workshopsIds = workshopsCursor.map(function(w) { return w._id });
			// console.log(workshopsIds);
			// var e = Enrolments.find({ year:year, month:month, workshopId: { $in: workshopsIds } })
			// //console.log(w.fetch());
			// console.log(e.fetch()); 
			//var c =  Enrolments.find({month:month, year:year, workshop:{days:1}});
			//console.log(enrolments); 
			return enrolments;
		},

		'enrolments.findByWeekDay2' (month, year, oneClass) {
			console.log('enrolments.findByWeekDay2');
			console.log(month);
			console.log(year);
			month = parseInt(month);
			year = parseInt(year);
			var pipeline = (
				[
					{ $match: { month: month, year:year} },
				    { 
				        $unwind: {
				            path: '$payments',
				    		preserveNullAndEmptyArrays: true
						}
					},
				    {
					  $group:
					  {
						   _id: "$_id",
						   fee: {$first: '$fee'},
						   paymentsSum: { $sum: "$payments.amount" },
						   attendances: {$first: "$attendances"},
						   contactId: {$first: "$contactId"},
						   workshopId: {$first: "$workshopId"}
					  }
					},
					{ $unwind: '$attendances' },
								
					{
					  $group:
					  {
						   _id: {enrolmentId:"$_id", weekDay: "$attendances.weekDay", workshopId:"$workshopId"},
						   att: {
						       $push:  {
						           day: "$attendances.day",
						           status:'$attendances.status',
						       }
						   },
						   fee: {$first: '$fee'},
						   paymentsSum: {$first: "$paymentsSum"},
						   contactId: { $first: "$contactId"}
					  }
					},
					{
						$lookup:
						{
						   from: 'contacts',
						   localField: 'contactId',
						   foreignField: '_id',
						   as: 'contact'
						}
					},
					{ $unwind: '$contact' },
					{
						$group:
						{
						   _id: {weekDay: "$_id.weekDay", workshopId: "$_id.workshopId"},
						   workshopId: {$first: '$_id.workshopId'},
						   enrolments: { $push:  {
						           enrolmentId: '$_id.enrolmentId',
				    		       contactId:'$contactId',
				    		       contact:'$contact',
				    		       days: '$att',
				    		       paymentsSum: '$paymentsSum',
				    		       fee: '$fee',
						       }
						   }
						}
					},
					{
						$lookup:
						{
						   from: 'workshops',
						   localField: 'workshopId',
						   foreignField: '_id',
						   as: 'workshop'
						}
					},
					{ $unwind: '$workshop' },
					{
						$group:
						{
						   _id: {weekDay: "$_id.weekDay"},
						   workshops: { $push:  {
						        workshopId: "$_id.workshopId", 'workshop':'$workshop',
						        enrolments:'$enrolments', 
						      } 
						   }
						}
					},
								
					{ $sort : { '_id.weekDay' : 1} }
				]
			);
			var data = Enrolments.aggregate(pipeline);
			result = [];
			for (var i = 0; i < 7; i++) {
				var obj = {weekDay: i,workshops:[]};
				//console.log('data');
				//console.log(data);
				var index = _.findIndex(data,{'_id': {'weekDay':i} })
				if(index>=0){
					obj.workshops = data[index].workshops;
				}
				result.push(obj);
			}
			// //console.log(result);
			 return result;
		}
	});
}

//////////////
/// Enrolments helper functions
///////////////
/**
 * Returns true if oldEnrolment and newEnrolment are different in any of this properties:
 * month, year, workshopId, fee, contactId, attendances.lenght, peyments
 * @param  {Object} oldEnrolment old Enrolment Object
 * @param  {Object} newEnrolment new Enrolment Object
 * @return {Boolean}             true if there is any difference, false if all are equal
 */
export function checkIfChanged(oldEnrolment, newEnrolment){
  if(oldEnrolment.month!=newEnrolment.month){
    return true;
  }else if(oldEnrolment.year!=newEnrolment.year){
    return true;
  }else if(oldEnrolment.workshopId!=newEnrolment.workshopId){
    return true;
  }else if(oldEnrolment.fee!=newEnrolment.fee){
    return true;
  }else if(oldEnrolment.contactId!=newEnrolment.contactId){
    return true;
  }else if(oldEnrolment.attendances.length!=newEnrolment.attendances.length){
    return true;
  }else{
    return (JSON.stringify(oldEnrolment.payments) != JSON.stringify(newEnrolment.payments));              
  }
  return false;
}; 