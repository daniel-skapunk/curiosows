import moment from 'moment';
moment.locale("es");
import _ from 'lodash';

export const curiosoPerc = .35;
export const fee = 600;
export const routesThatDontRequireAuth = ['agreement'];

export const weekDays = [
  {lbl:'Lunes', val:1},
  {lbl:'Martes', val:2},
  {lbl:'Miercoles', val:3},
  {lbl:'Jueves', val:4},
  {lbl:'Viernes', val:5},
  {lbl:'Sabado', val:6},
  {lbl:'Domingo', val:0}
];
export const months = [
    {lbl:'Enero', val:1},
    {lbl:'Febrero', val:2},
    {lbl:'Marzo', val:3},
    {lbl:'Abril', val:4},
    {lbl:'Mayo', val:5},
    {lbl:'Junio', val:6},
    {lbl:'Julio', val:7},
    {lbl:'Agosto', val:8},
    {lbl:'Septiembre', val:9},
    {lbl:'Octubre', val:10},
    {lbl:'Noviembre', val:11},
    {lbl:'Diciembre', val:12}
];

var currentYear = new Date().getFullYear();
var diffYears = currentYear - 2015 + 1;
export const years = Array.from(new Array(diffYears), (x,i) => i + 2015);
/**
 * routeClean checks if the route is in the list of routesThatDontRequireAuth
 * @param  {[type]} route [description]
 * @return {[type]}       [description]
 */
export function routeClean(route) {
  var routesThatDontRequireAuth = this.routesThatDontRequireAuth;
  console.log('routesThatDontRequireAuth',this)
  return _.find(routesThatDontRequireAuth,
    function (noAuthRoute) {
      return route.localeCompare(noAuthRoute) == 0;
    });
};

/**
 * getAttendanceArray
 * Returns an array of Objects where each object is an attendance
 with day, weekday and status (-1:no , 0:notset, 1:yes)
 * @param  {Bumber} month    month to work on
 * @param  {Number} year     year to work on
 * @param  {Array} weekDays  array of weekDays ex: [0,2] (Mon, Wed)
 * @return {Array.<{day: Number, weekDay: Number, status: Number}>}
 */
export function getAttendanceArray(month, year, weekDays ){
  //console.debug('helperAttendanceArray');
  if(!month || !year ) return;
  
  // calculate start and end date for this month and year
  var start = moment(year+'-'+month+'-01'); //var start = moment($scope).add(1, 'day');
  var daysInMonth = moment(year+'-'+month).daysInMonth();
  var end = moment(year+'-'+month+'-'+daysInMonth); //var end = moment(date).subtract(1, 'day'), weekdays, index
  
  //_days = _.map(weekDays, function(num){ return num + 1; });
  var _days = weekDays;
  var attendanceDates = weekDaysIn(start, end, _days, null);
  // status default to 0
  var attendances = _.map(attendanceDates, function(_date){
    return {day: moment(_date).date(), weekDay:moment(_date).day(), status:0};
  });
  return attendances;
}



export function getErrorFunction(successMessage, alertService, $scope, errFnc, succFnc){
  var e = function(error, result){
    if(error){
      console.debug('error'); console.debug(error);
      alertService.alert({message: error.reason, type:'danger'});
      errFnc();
    }else{
      console.debug('success');
      alertService.alert({message: successMessage, type:'success'});
      succFnc();
    }
    $scope.$apply();
  }
  return e;
}

export function attArrToCalendar(month, year, attArr){
  console.debug(month);
  console.debug(year);
  console.debug(attArr);
  var calendarSelectedDates = _.map(attArr, function(d){
    //froce 2 digits: ("0" + myNumber).slice(-2)
    obj = year + '-' + month + '-' + ("0" + d.day).slice(-2);
    return moment(obj);
  });
  return calendarSelectedDates;
}

export function calendarToAttArr(calendarSelDates, attArr){

  var attArr = _.map(calendarSelDates,function(d){
        day = moment(d).format('DD');
        weekDay = moment(d).day()-1;
        dayfound = _.find(attArr, function(o) { return o.day == day; });
        status = dayfound? dayfound.status:-1;
        return {day:day, weekDay:weekDay , status: status}
  });
  return attArr;
}

/**
 * Given a array of weekDays, returns array of weekDays in readable format
 *
 * @param  {Array}  days ex: [1,2,3] for Mar, Mie, Jue
 * @return {Array}  readable array of weekDays ex: {Mar, Mie, Jue}
 */
export function daysToString(days,len){
    if(!days) return '';
    var r = '';
    for (var i = 0; i < days.length; i++) {
      if(i>0) r += ' ';
      r += dayToString(days[i],len);
    }
    return r;
}

/**
 * Given a array of weekDays, returns array of weekDays in readable format
 * 
 * @param  {Array}  day ex: 1 for Lunes
 * @param  {Array}  len ex: 3 for Lun, default: 3
 * @return {Array}  readable array of weekDays ex: {Mar, Mie, Jue}
 */
export function dayToString(day, len){
  if(day==null) return "falta día"
  var res;
  var r = weekDays.find( wd => wd.val === day ); 
  if(len){
    res = r.lbl.substring(0,len);
  } else res = r.lbl

  return res;
}


/**
 * Given a data and an array of WeekDays,
 * finds previous(default) or next (and same optionally) date of the closest weekdate
 * ex: daysArray [1,4] (Ma,Vie), adate Mi 25 Mar 2020, returns Ma 24 Mar 2020 
 *
 * @param  {Array}  daysArray ex: [1,2,3] for Mar, Mie, Jue
 * @param  {Array}  adate ex: the date to find the colsest
 * @return {Moment Date} previous or next closest date  
 */
export function findClosestDateInDaysArray(daysArray, adate, previous, same) {
  console.log('findClosestDateInDaysArray: ');
  if(same==null) same = false;
  if(typeof previous == 'undefined') previous = true;
  if(!daysArray.length) return;
  if(!moment.isMoment(adate)){
    adate = moment(adate);
  }
  var aday = adate.day();
  var closest = getNextOrPrevClosestInt(daysArray, aday, previous, same, true);
  
  var result;
  // GETS PREVIOUS
  if(previous){
    if(same && aday==closest)
      return adate;
    if(closest > aday){
      // goto previous week
      adate.subtract(7, 'days');
    }
  } else {
    //console.log('next!!: ');
    // GETS NEXT
    // goto previous week
    if(closest < aday)
      adate.add(7, 'days');
  }
  result = adate.day(closest);
  return result;
}


// getNextOrPrevClosestInt
// Get the closest next or previous integer in a sorted array of integers
// array = sorted array of integers
// val = pivot element
// prev = boolean, if true, returns the previous value
export function getNextOrPrevClosestInt(array, val, prev, same, cycle) {

  for (var i=0; i < array.length; i++) {
    //if (prev == true) {
      //if (array[i] == val) return array[i];
      if (val < array[i]){
        break;
      } 
      else if(val==array[i]){
        if(same) 
          return array[i];
        else{
          if(!prev) i++;
          break;
        }
      }
  }

  // quiere anterior
  if(prev){
    if( (i-1) >= 0){ 
      // si hay anterior
      return array[i-1];
    } else{
      // no hay anterior
      if(cycle) // return last element
        return array[array.length-1]
      else // no previous value return null
        return null;
    }
  } 
  // quiere siguiente
  else {
    if(i < array.length){ 
      // si hay sieguiente
      return array[i];
    } else{
      // no hay siguiente
      if(cycle)
        return array[0];
      else
        return null;
    }
  }
}

// FROM:
//! moment-weekdaysin.js
//! version : 1.0.1
//! author : Kodie Grantham
//! license : MIT
//! github.com/kodie/moment-weekdaysin
/**
 * Gets the dates between start and end dates, that satisfies weekdays array condition
 * @param  {[type]} start    start date
 * @param  {[type]} end      end date
 * @param  {[type]} weekdays array of weekdays to fetch
 * @param  {[type]} index    index based ie:
 *
 * Last Wednesday
 * var lastWednesday = moment().dayOfYear(1).weekDaysIn(start, end, 3, -1);
 *
 * @return {[type]}          array of dates
 */
export function weekDaysIn(start, end, weekdays, index) {
    var days = [], d = moment(start).startOf('day');
    var isIndexed = (typeof index !== 'undefined' && index !== null);

    if (typeof weekdays !== 'undefined' && weekdays !== null) {
      if (weekdays.constructor !== Array) { weekdays = [weekdays]; }

      for (var w = 0; w < weekdays.length; w++) {
        weekdays[w] = moment(start).day(weekdays[w]).day();
      }
    } else {
      weekdays = [0,1,2,3,4,5,6];
    }

    for (var i = 0; i < (moment(end).endOf('day').diff(moment(start), 'days') + 1); i++) {
      var wd = d.day();

      if (isIndexed && !days[wd]) { days[wd] = []; }

      if (weekdays.indexOf(wd) !== -1) {
        if (isIndexed) {
          days[wd].push(moment(d));
        } else {
          days.push(moment(d));
        }
      }

      d.add(1, 'day');
    }

    if (isIndexed) {
      var nDays = [];

      if (index.constructor !== Array) { index = [index]; }

      for (var a = 0; a < days.length; a++) {
        if (!days[a].length) { continue; }

        for (var n = 0; n < index.length; n++) {
          var ind = parseInt(index[n]);
          if (isNaN(ind)) { continue; }
          var ni = (ind - 1);
          if (ind < 0) { ni = (days[a].length + ind); }
          nDays.push(days[a][ni]);
        }
      }

      days = nDays;
    }

    days = days
      .sort(function(a, b){ return moment.utc(a).diff(moment.utc(b)); })
      .filter(function(o, p, a){ return (o != null && !o.isSame(a[p - 1])); });

    if (!days.length) { return false; }
    if (days.length === 1) { return days[0]; }

    return days;
 };

 /**
 * getWeekDaysInMonth get Week Days in month, returns array of mondays, tuesdas, etc...
 * example = [{weekDay:0, days:[7,14,21,28]},[...]]
 * @param  {Number} month the month
 * @param  {Number} year  the year
 * @return {Array}       Seven day array [{weekDay:0, days:[7,14,21,28]},[...]]
 */
export function getWeekDaysInMonth(month, year){
  res = _.times(7, function(_index){
      console.debug(year);
      var start = moment(year+'-'+month+'-01'); //var start = moment($scope).add(1, 'day');
      daysInMonth = moment(year+'-'+month).daysInMonth();
      var end = moment(year+'-'+month+'-'+daysInMonth); //var end = moment(date).subtract(1, 'day'), weekdays, index
      var wd = weekDaysIn(start, end, _index+1);
      wd = _.map(wd, function(value){
        return moment(value).format('DD');
      });
      obj = {weekDay: _index, days: wd };
      console.debug(obj);
      return obj;
  })
  return res;
}

export function mergeAttendances(source, target){
  if(!source || !target ||
    !source.length || !target.length) {
    if(source.length>0)
      return source;
    else if (target.length>0)
      return target;
    else return [];
  }

  if( source[0].month!=target[0].month && 
    source[0].year!=target[0].year) return 'dont have same month or year';
  var newArray = source.concat(target);
  // source has presedence
  _.uniqBy(newArray, 'day');
  return newArray;
}

export function mergeDays(days1, days2){
  return _.union(days1, days2);
}