import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Contacts } from '../api/contacts.js';

import * as helpers from '../helpers.js';

import template from './contactsAddForm.html';
import templateAdd from './contactsAdd.html';
import templateEdit from './contactsEdit.html';

class ContactsAddEditFormCtrl {
	constructor($scope, $state, $stateParams, alertService){
		$scope.viewModel(this);
		this.$state = $state;
		this.$scope = $scope;
		this.newContact = {};
		this.alertService = alertService;
		
		if($stateParams.contactId){
			this.newContact = Contacts.findOne({_id:$stateParams.contactId});
		}

		this.helpers({
		})
	}
	addContact(newContact) {
		var successMessage = newContact._id ? "Contacto Actualizado" :  "Contacto Agregado";
		Meteor.call('contacts.insertupdate', newContact,
			helpers.getErrorFunction(successMessage, this.alertService, this.$scope,
				()=>{},
				()=>{
					this.newContact = "";
					this.$state.go('contacts');
				}
			)
		);
	}
	cancel(){
		this.$scope.$emit('goBack');
	}
}

export default angular.module('contactsAddForm',[
	angularMeteor
])
 .component('contactsAdd', {
	templateUrl: 'imports/components/contactsAdd.html',
	//controller: ['$scope', '$state','alertService',ContactsAddEditFormCtrl]
 })
 .component('contactsEdit', {
	templateUrl: 'imports/components/contactsEdit.html',
	//controller: ContactsAddCtrl
 })
 .component('contactsEditForm', {
	templateUrl: 'imports/components/contactsAddForm.html',
	controller: ['$scope', '$state', '$stateParams', 'alertService', ContactsAddEditFormCtrl]
 })
 .component('contactsAddForm', {
	templateUrl: 'imports/components/contactsAddForm.html',
	controller: ['$scope', '$state', '$stateParams', 'alertService', ContactsAddEditFormCtrl]
 });