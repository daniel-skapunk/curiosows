import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Enrolments } from '../api/enrolments.js';
import { Workshops } from '../api/workshops.js';
import { Contacts } from '../api/contacts.js';

import * as help from '../helpers.js';

import template from './callrollEnrolmentsList.html';

import moment from 'moment';
moment.locale("es");

class CallrollEnrollmentsList {
	constructor($scope, $reactive, $rootScope, alertService){
		//$scope.viewModel(this);
		$reactive(this).attach($scope);
		this.subscribe('enrolments');
		this.$scope = $scope;

		this.fee = help.fee;
		
		// to access this inside watches and other places
		var thi = this;

		this.selected = ''; //selected contact when added new 

		// LIBRARIES
		//this.$rootScope = $rootScope;
		this.alertService = alertService;
		// copia months y years de rootscope y borra el elemento vacío de months;
		//this.weekDays = help.weekDays.slice(); // copia weekDays de help

		// BINDINGS
		this.workshopId;
		this.date // binding with component
		this._date; // date converted to moment
		this.month;
		this.year;
		this.day;
		this.weekday;
		// Month year and day will be updated when date changes.
		//this.updateDateVars();
		//this.workshop;

		//JS DATE monts 0-11 weekdays 0-Dom 6-Sab

		this.helpers({
			enrolments: function(){
				//console.log('getEnrolments callrollEnrolmentsList!!!');
				var enr; //result var
				var date = this.getReactively( 'day' );
				var day = this.getReactively( 'day' );
				var month = this.getReactively( 'month' );
				var year = this.getReactively( 'year' );
				var weekday = this.getReactively( 'weekday' );
				var workshopId = this.getReactively( 'workshopId' );
				
				// fill search object
				var srchObj = {};
				if(month){ srchObj.month = month; }
				if(year){ srchObj.year = year; }
				if(workshopId){ srchObj.workshopId = workshopId; }
				srchObj.days = weekday;

				//// fill options object, bring only teacher and attendances array
				var options = { fields: {"contactId":1, "attendances":1, "days":1, 'workshopId':1}};
				
//				console.debug(srchObj);
				enr = Enrolments.find(srchObj,options); // get Enrolments
				return enr;
			},
			workshop: function() {
				var workshopId = this.getReactively( 'workshopId' );
				var workshop;
				//console.log('workshopId: ', workshopId);
				if(workshopId){
					workshop = Workshops.findOne({_id: workshopId},{days:1});
					//console.log('workshop: ', workshop);
				}
				return workshop;
			},
			contacts(){
				var str = this.getReactively('selected');
				//console.debug(this.getReactively('selected'));
				//return Contacts.find({first_name:str});
				//var contacts = Contacts.find({first_name: { $regex: str } })
				var contacts = Contacts.find({ $or: [
					{first_name: { $regex: str, $options: 'i' } },
					{last_name: { $regex: str, $options: 'i' } }
					]
				}); 
				//console.debug(contacts);
				return contacts;
				//Meteor.call('contacts.find', str);
			}
		})

		this.autorun(() => {
			//console.debug('autorun');
			var month = this.getReactively( 'search.month' );
			var year = this.getReactively( 'search.year' );
			this.getReactively('workshopId');
			//console.debug(month);
			this.subscribe('enrolments', function(){ return [month, year]; } , {
				onStart: function(){
					$rootScope.loading = true;
				},
				onReady: function(){
					//thi.prev();
					$rootScope.loading = false;
					// var wsIds = this.enrolments.map(function(p) { return p.workshopId });
					// var contactIds = this.enrolments.map(function(p) { return p.contactId });
				}
			}); // subscribe workshops
		});

		$scope.$watch('$ctrl.date', function(newValue, oldValue){
			if(newValue!=null)
				thi._date = moment(newValue);
			thi.updateDateVars()
		}, true);

		$scope.$watch('$ctrl.workshopId', function(newValue, oldValue){
			//console.log('workid: ');
		}, false);
	}

	/**
	* Update date vars, execute it when date changes
	*/ 
	updateDateVars(){
		//console.log('this.date: ', this._date);
		this.month = this._date.month()+1;
		this.year = this._date.year();
		this.day = this._date.date();
		this.weekday = this._date.day();
	}

	typeaheadOnSelect($item, $model, $label, $event){
		var contactId = $item._id;
		var attendances = help.getAttendanceArray(
			this.month,
			this.year,
			[this.weekday]);
		var newEnrolment = {
			contactId: contactId, workshopId: this.workshopId,
			days:[this.weekday], month: this.month, year: this.year,
			paymentsSum: 0, oneClass: false, oneClassDate:null,
			fee: this.fee, payments:[],
			attendances: attendances
		};
		//console.log('newEnrolment: ', newEnrolment);
		var successMessage = "Alumno agregado";
		this.call('enrolments.insertUpdateIfExists', newEnrolment, 
			help.getErrorFunction(successMessage, this.alertService, this.$scope,
				()=>{},
				()=>{
					this.newEnrolment = "";
					this.selected="";
					// this.$state.go('enrolments');
				}
			)
		);
	}

	/**
	 * @param  {Enrolment} The enrolment to change attendance
	 */
	changeAttendance(enrolment){
		var attendances = enrolment.attendances;
		var result = this.findAttendance(enrolment,this.day);
		if(result=='error'){ this.alertService.alert('ERROR!') }

		// if status 1 or -1 change it, if 0 set it to 1
		result.status = result.status ?  result.status * -1 : 1;

		Meteor.call('enrolments.updateAttendances',enrolment._id, this.day, result.status);
	}

	findAttendance(enrolment, day){
		var result = enrolment.attendances.find( att => att.day === day );
		if(result) return result;
		else {
			var d = {
				day: day,
      	weekDay: this.weekday,
      	status: 0
			};
			enrolment.attendances.push(d);
			result = enrolment.attendances.find( att => att.day === day );
			// return 'error';
			return result; 
		}
	}

	// isPresent(enrolment, day){
	// 	var result = enrolment.attendances.find( att => att.day === day );
		
	// }
	isPresentClass(enrolment,day){
		var att = this.findAttendance(enrolment,day)
		var r = att.status;
		if(r == -1) return 'absent';
		if(r == 1) return 'present';
		if(r == 0) return 'undefined';
	}
	
	daysToString(days){
		return help.daysToString(days,3);
	}

	sortBy(propertyName) {
    	this.reverse = (propertyName !== null && this.equals(this.propertyName, propertyName) )? !this.reverse : false;
    	this.propertyName = propertyName;
    	//this.friends = orderBy(friends, this.propertyName, this.reverse);
  	};
  	equals(a,b){
  		return angular.equals(a,b);
  	};
  	getMonth(month){
  		if(month){
  			//console.debug(month);
  			return this.months[month].lbl;
  		}
  	};
  	filterBy(year_month){
  		if(!!year_month.year == false)
  			delete year_month.year;
  		//console.debug(year_month);
  		return year_month;
  	};

  	getDay(days,day){
  		
  	};
}

export default angular.module('callrollEnrolmentsList',[
	angularMeteor
])
 .component('callrollEnrolmentsList', {
	templateUrl: 'imports/components/callrollEnrolmentsList.html',
	controller: ['$scope', '$reactive', '$rootScope', 'alertService', CallrollEnrollmentsList],
	bindings: {
		orderBy: '@',
		month: '@',
    	contactId: '@',
    	workshopId: '@',
    	date: '@'
  	}
 });



