import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Enrolments } from '../api/enrolments.js';
import { Workshops, WorkshopsHelpers } from '../api/workshops.js';
import { Contacts } from '../api/contacts.js';

import * as help from '../helpers.js';

import template from './dashboardView.html';

class DashboardCtrl {
	constructor($scope, $reactive, $rootScope){
		$scope.viewModel(this);
		$reactive(this).attach($scope)
		this.subscribe('contacts');
		//this.subscribe('enrolments');

		this.months = help.months.slice();
		this.years = help.years.slice();
		this.weekDays = help.weekDays.slice();

		this.curiosoPerc = help.curiosoPerc; 
		
		this.search = {
			month: new Date().getMonth()+1,
			year: new Date().getFullYear()
		};
		//this.workshopId = '6JjBk8EFPm3hbtXkG';
		this.data = {};
		this.totalActualIncome = 0;
		this.totalEstimatedIncome = 0;

		this.selected = '';  // string to search contact typeahead
		this.attendance = [];

		var scope = $scope;
		var parent = this;

		$scope.$watch('$ctrl.data', function(newValue, oldValue){
			if (!newValue) return;
			
			parent.totalActualIncome = 0;
			parent.totalEstimatedIncome = 0;
			
			for (var i = 0 ; i < newValue.length ; i++) {
				//console.debug('data');
				//console.debug(newValue[i]);
				parent.totalEstimatedIncome+=newValue[i].income;
				parent.totalActualIncome+=newValue[i].actualIncome;
			}
		}, true);

		this.helpers({
			contacts(){
				var str = this.getReactively('selected');
				//console.debug(this.getReactively('selected'));
				//return Contacts.find({first_name:str});
				//var contacts = Contacts.find({first_name: { $regex: str } })
				var contacts = Contacts.find({ $or: [
					{first_name: { $regex: str, $options: 'i' } },
					{last_name: { $regex: str, $options: 'i' } }
					]
				}); 
				//console.debug(contacts);
				return contacts;
				//Meteor.call('contacts.find', str);
			}
		})

		this.autorun(() => {
			var month = this.getReactively( 'search.month' );
			var year = this.getReactively( 'search.year' );
			Meteor.call('enrolments.count', month,year, false, function(err, data){
				//angular.extend(parent.enrolmentsWeek, data);
				//console.debug(data);
				parent.data = data;
				console.debug(data);

				scope.$apply();
			});

			var month = this.getReactively( 'search.month' );
			var year = this.getReactively( 'search.year' );
			this.subscribe('enrolments', function(){ return [month, year]; } , {
				onReady: function(){
				}
			}); // subscribe workshops
		});
  	} // CONSTRUCTOR END

  	/////////////////
  	/// Class Methods
  	//////////////////
  	typeaheadOnSelect($item, $model, $label, $event){
		var e = Enrolments.find({contactId:$item._id, workshopId:'6JjBk8EFPm3hbtXkG'}, {fields:{fee:1,payments:1}});
		$item.enrolment = e.fetch()[0];
		console.debug($item.enrolment);
		this.attendance.push($item);
		//console.debug(typeof _);
		this.selected = '';
	}
	workshopToString(workshop, nDays=null){
		if(!workshop) return "sin taller";
		var r = WorkshopsHelpers.workshopToString(workshop, nDays);
		return r;
	}
}

export default angular.module('dashboard',[
	angularMeteor,
	uiRouter
])
 .component('dashboard', {
	templateUrl: 'imports/components/dashboardView.html',
	controller: ['$scope', '$reactive', '$rootScope',DashboardCtrl],
	bindings: {
    	//contactId: '@',
  	}
 });



