import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Workshops } from '../api/workshops.js';
import { Contacts } from '../api/contacts.js';

import * as help from '../helpers.js';

import template from './callroll.html';

import moment from 'moment';
moment.locale("es");

class CallrollCtrl {
	constructor($scope, $rootScope, $reactive, $stateParams, alertService){
		'ngInject';
		$reactive(this).attach($scope);
		this.subscribe('workshops');
		this.subscribe('contacts');

		// to access this inside watches and other places
		var thi = this;
		this.alertService = alertService;
		this.$rootScope = $rootScope;

		// this.contactId = $stateParams.contactId;
		this.contactId = ''
		
		this._date = moment();
		// Month year and day will be updated when date changes.
		this.updateDateVars();
		
		this.workshops = [];
		this.teacher;
		this.days = [];		// weekdays result of merging this teacher workshop.days

		// true: showsAll workshops
		this.showAll=false;
		// if(Roles.userIsInRole ( Meteor.user, 'admin')){
		// 	this.showAll=true;
		// }

		this.helpers({
			workshops(){
				// console.log($rootScope.currentUser?.profile?.contactId)
				var currentUser = this.getReactively('$rootScope.currentUser')
				var contactId = this.getReactively('contactId');
				var showAll = this.getReactively('showAll');
				contactId = $rootScope.currentUser?.profile?.contactId
				var query = {teacherId: contactId, archived:{ $ne: true }}
				if(showAll) delete query.teacherId;
				console.log(query)
				var wss = Workshops.find(query);
				if(wss){
					// Hacer un merge de los arrays de talleres de este maestro
					// para hacer bien el cambio de días
					this.days = [];
					wss.forEach((ws) => {
						this.days = _.union(this.days, ws.days);	
					});
					// ordena el array
					this.days = this.days.sort( (a,b)=> a - b);
					//this._date = moment();
					// this.prev();
				} 
				return wss;
			},
			teacher(){
				var contactId = this.getReactively('contactId');
				return this.teacher = Contacts.findOne({_id: contactId});
			}
		});

		$scope.$watch('$ctrl._date', function(newValue, oldValue){
			if(newValue!=null){
				console.log('updateDateVars')
				thi.updateDateVars();
			}
		}, true);
		$rootScope.$watch('currentUser', function(newValue, oldValue){
			console.log('User changed!', newValue);
			if(Roles.userIsInRole ( Meteor.user, 'admin')){
				console.log('ADMINNNNN!')
				thi.showAll=true;
			}
		}, true);
	}

	/**
	* Update date vars, execute it when date changes
	*/
	updateDateVars(){
		this.month = this._date.month()+1;
		this.year = this._date.year();
		this.day = this._date.date();
		this.weekday = this._date.day();
	}

	dayToString(day, num){
		return help.dayToString(day, num);
	}

	monthToString(month){
		return help.months[month].lbl;
	}

	prev(){
  		if(!this._date || !this.workshops.length){ 
  			this.alertService.error('Falta fecha o taller');
  			return;
  		}
  		var d = help.findClosestDateInDaysArray(this.days, this._date)
  		this._date = d;
  	};
  	next(){
  		if(!this._date || !this.workshops){
  			this.alertService.error('Falta fecha o taller');
  			return;
  		}
  		var d = help.findClosestDateInDaysArray(this.days, this._date, false)
  		console.log('d: ', d);
  		this._date = d;
  	};

  	isWorkshopInThisDay(ws, day){
  		// console.log(ws, ws.days);
  		if(!ws.days) {
  			// ws has no days set, dont include it
  			return false;
  		}
  		return ws.days.includes(day);
  	}
  	toggleShowAll(){
  		console.log(Meteor.user().profile.contactId)
  		this.contactId = this.contactId == 'all'? Meteor.user().profile.contactId : 'all';
  	}
  	canISee(role){ return this.$rootScope.canISee(role) }
}

export default angular.module('callroll',[
	angularMeteor,
	uiRouter,
])
 .component('callroll', {
	templateUrl: 'imports/components/callroll.html',
	controller: ['$scope','$rootScope','$reactive', '$stateParams', 'alertService', CallrollCtrl]
 });