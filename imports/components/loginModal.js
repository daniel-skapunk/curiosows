export function loginModalCtrl($uibModalInstance, $state, alertService) {
		var $ctrl = this;
		this.alertService = alertService;
		var _user = {
			rememberMe: true
		};

		$ctrl.login = function(loginData) {
			console.debug('login modal!!!');
			console.debug(loginData);

			Meteor.loginWithPassword(loginData.email, loginData.password, function(err){
				if(!err){
					// console.debug('loggeado');
					if($ctrl.canISee('admin'))
						$state.go('enrolments');
					else if($ctrl.canISee('teachers'))
						$state.go('callroll');
					$uibModalInstance.close(Meteor.user());
				}else{
					console.debug('alertService', alertService);
					// console.log(err)
					$ctrl.alertService.alert({message: 'Usuario o Contraseña incorrectos', type:'danger'});
					// console.debug(err);
					// alertService.secondayAlert({type:'danger', message:value.data.error.message});	
				}
			});

			// User.login(_user, loginData, function(value, responseHeaders){
			// 		console.debug(value);
			// 		if(!value.id){
			// 			alertService.secondayAlert({type:'danger', message:value.data.error.message});	
			// 		}else{
			// 			$uibModalInstance.close(value.user);
			// 		}
			// 		// console.debug(value);
			// 		// console.debug(responseHeaders);
			// 	}, function(httpResponse){
	 		// 		alertService.secondaryAlert({type:'danger', message:httpResponse.data.error.message});
			// 		//console.debug(httpResponse);
			// });
		};

		$ctrl.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

		$ctrl.closeAlert = function(index) {
			alertService.closeSecondaryAlert(index);
		};
}