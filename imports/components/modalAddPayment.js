import angular from 'angular';
import angularMeteor from 'angular-meteor';


import template from './modalAddPayment.html';

class ModalInstanceCtrl {
	constructor($scope, $state, $uibModalInstance){
		$scope.viewModel(this);
		this.$state = $state;
		this.amount = 0;

		this.helpers({
		})
	}
	ok(){
		$uibModalInstance.close(this.amount);
	}

	cancel(){
		$uibModalInstance.dismiss('cancel');
	}
}

export default angular.module('modalAddPayment',[
	angularMeteor
])
 .component('modalAddPayment', {
	templateUrl: 'imports/components/modalAddPayment.html',
	controller: ['$scope', '$state', '$uibModalInstance', ModalInstanceCtrl]
 });