import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Enrolments } from '../api/enrolments.js';

import template from './enrolmentsWeekMonthly.html';

import * as helpers from '../helpers.js';

import moment from 'moment';
moment.locale("es");

class EnrolmentsWeekMonthlyCtrl {
	constructor($scope, $reactive, $rootScope, alertService){
		console.debug('EnrolmentsWeekMonthlyCtrl');
		
		$scope.viewModel(this);
		$reactive(this).attach($scope);
		// this.subscribe('enrolments');
		this.$scope = $scope;
		this.alertService = alertService;

		this.month = this.month ? this.month : moment().month() + 1;
		this.year = this.year ? this.year : moment().year();
		console.debug(this.month);
		console.debug(this.year);

		this.enrolmentsWeek;
		this.weekDays = helpers.weekDays;

		var p = this;
		
		this.weekDaysInMonth = helpers.getWeekDaysInMonth(this.month, this.year)
		console.debug('this.weekDaysInMonth');
		console.debug(this.weekDaysInMonth);

		//this.findEnrolmentsByWeek(this.month,this.year,$scope);
		this.findEnrolmentsByWeek(this.month,this.year,$scope);

		
		this.helpers({
			enrolments: function(){
				var enr;
				var month = this.getReactively( 'search.month' );
				var year = this.getReactively( 'search.year' );
				var contactId = this.getReactively( 'contactId' );
				var workshopId = this.getReactively( 'workshopId' );
				var srchObj = {};
				if(month){ srchObj.month = month; }
				if(year){ srchObj.year = year; }
				if(contactId){ srchObj.contactId = contactId; }
				if(workshopId){ srchObj.workshopId = workshopId; }
				//console.debug(srchObj);
				enr = Enrolments.find(srchObj);
				//console.debug(enr);
				return enr;
			},
		})

		this.autorun(() => {
			month = this.month; year = this.year;
			this.subscribe('enrolments', function(){ return [month, year]; } , {
				onStart: function(){
					$rootScope.loading = true;
				},
				onReady: function(){
					$rootScope.loading = false;
					//$scope.$apply();
				}
			});
		});
	}

	$onChanges(changesObj){
		// if month or year changed
		if(changesObj.month || changesObj.year){
			this.findEnrolmentsByWeek(this.month,this.year,this.$scope);
		}
	}

	findEnrolmentsByWeek(month, year, scope){ 
		console.debug('findEnrolmentsByWeek');
		var parent = this;
		Meteor.call('enrolments.findByWeekDay2', month, year, false, function(err, data){
			console.debug('findEnrolmentsByWeek data');
			console.debug(data);
			parent.enrolmentsWeek = data;
			scope.$apply();
		});
	}

	payed(enrolment){
		//console.debug(enrolment);
		if(!enrolment.paymentsSum)
			return false;
		if(enrolment.paymentsSum < enrolment.fee)
			return false;
		else
			return true;
	}
  	changeAttendance(enr,attendances, day){
  		//found =_.map([1, 2, 3], function(num){ return num * 3; });
  		//console.debug(day);
  		if(day.status==null || day.status==-1)
  			day.status = 1;
  		else if(day.status == 1)
  			day.status = 0;
  		else if(day.status == 0)
  			day.status = -1;
  		Meteor.call('enrolments.updateAttendances',enr.enrolmentId, day.day, day.status);
  	};

  	getDay(days,day){
  		console.debug('getDay');
  		console.debug(days);
  		console.debug(day);
  		for (var i = 0; i < days.length; i++) {
  			d = days[i].day;
  			if(d==day) return days[i];
  		}
  		return false;
  	}
}

export default angular.module('enrolmentsWeekMonthly',[
	angularMeteor
])
 .component('enrolmentsWeekMonthly', {
	templateUrl: 'imports/components/enrolmentsWeekMonthly.html',
	controller: ['$scope', '$reactive', '$rootScope', 'alertService', EnrolmentsWeekMonthlyCtrl],
	bindings: {
		//orderBy: '@',
		month: '@',
		year: '@',
    	//contactId: '@',
    	//workshopId: '='
  	}
 });



