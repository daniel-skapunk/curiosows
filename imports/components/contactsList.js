import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import utilsPagination from 'angular-utils-pagination';
import { Counts } from 'meteor/tmeasday:publish-counts';

import { Contacts } from '../api/contacts.js';

import template from './contactsList.html';

class ContactsListCtrl {
	constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);

		this.propertyName = 'first_name'; 
		this.reverse = false;

		this.perPage=10;
		this.page = 1;
		this.search={
			first_name:''
		};
		this.sort = {
      		first_name: 1
    	};
		//$scope.viewModel(this);

		this.helpers({
			contacts(){
				return Contacts.find({},{}); 
			},
			contactsCount() {
        		return Counts.get('numberOfContacts');
      		}
		})
		this.autorun(() => {
			// console.log('Autorun!!', this.getReactively('search.first_name'));
			this.subscribe('contacts',() => [
				{
					limit: parseInt(this.perPage),
					skip: parseInt((this.getReactively('page') - 1) * this.perPage),
					sort: this.getReactively('sort')
				}],
				{search: this.getReactively('search.first_name')}
			);
		});
	}
	sortBy(propertyName) {
    	this.reverse = (propertyName !== null && this.equals(this.propertyName, propertyName) )? !this.reverse : false;
    	this.propertyName = propertyName;
    	//this.friends = orderBy(friends, this.propertyName, this.reverse);
  	};
	equals(a,b){
  		return angular.equals(a,b);
  	};

	pageChanged(newPage){
		console.log(newPage);
		this.page = newPage;
	};

	deleteContact(contact){
		console.debug('delete');
		Meteor.call('contacts.remove', contact);
	};
}

export default angular.module('contactsList',[
	angularMeteor,
	uiRouter,
	utilsPagination
])
 .component('contactsList', {
	templateUrl: 'imports/components/contactsList.html',
	controller: ['$scope','$reactive', ContactsListCtrl]
 });