import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Contacts } from '../api/contacts.js';
//import { Users } from '../api/users.js';

import * as help from '../helpers.js';

import usersFormTemplate from './usersAddForm.html';
//import usersAddTemplate from './usersAdd.html';
//import usersEditTemplate from './usersEdit.html';

class UsersAddFormCtrl {
	constructor($scope, $rootScope, $reactive, $state, alertService){
		$scope.viewModel(this);
		$reactive(this).attach($scope)
		
		this.subscribe('usersList');
		this.subscribe('contacts');

		this.rootScope = $rootScope;

		this.$state = $state; 	// used to change routes
		this.$scope = $scope; 	// model for this ctrl
		this.alertService = alertService; // service to display alert messages
		
		this.contact;  			// associated contact to this user
		this.newUser = {profile:{}};	// user to be saved or edited

		this.contactSrch = '';	// str to search contact
		this.users;
		this.roles = ["admin", "teachers"]; // role array to select

		this.theUser = Meteor.user();  

		Meteor.call('users.find', (error, result) => {
			if(error)
				this.alertService.error(error.message);
			else{
				this.users = result; 
				this.alertService.success("Usuario Agregado");
				//this.$state.go('enrolments');
			}
		});
		//console.log('this.users: ', this.users);

		this.helpers({
			contacts(){
				var str = this.getReactively('contactSrch');
				//console.debug(this.getReactively('selected'));
				//return Contacts.find({first_name:str});
				//var contacts = Contacts.find({first_name: { $regex: str } })
				var contacts = Contacts.find({ $or: [
					{first_name: { $regex: str, $options: 'i' } },
					{last_name: { $regex: str, $options: 'i' } }
					]
				}); 
				//console.debug(contacts);
				return contacts;
				//Meteor.call('contacts.find', str);
			}
		});
	}

	typeaheadOnSelect($item, $model, $label, $event){
		this.newUser.email = $item.email;
		this.newUser.profile.first_name = $item.first_name;
		this.newUser.profile.last_name = $item.last_name;
		this.newUser.profile.contactId = $item._id;

		this.contact = $item;
	}
	saveRoles() {
		Meteor.call('users.saveRoles', this.users);
	}

	addUser(newUser) {
		Meteor.call('users.add', newUser, (error, result) => {
			if(error)
				this.alertService.error(error.message);
			else{
				this.alertService.success("Usuario Agregado");
				this.$state.go('enrolments');
			}
		});
		//Accounts.createUser(newUser, (error) => {console.log('error: ', error);})
		//
	}
	cancel(){
		this.$scope.$emit('goBack');
	}
	
}
export default angular.module('usersAddForm',[
	angularMeteor
])
 .component('usersAddForm', {
	templateUrl: 'imports/components/usersAddForm.html',
	controller: ['$scope', '$rootScope', '$reactive', '$state', 'alertService', UsersAddFormCtrl]
 });