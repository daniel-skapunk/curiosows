import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './login.html';

class LoginCtrl {
	constructor($scope,$state, $rootScope, $reactive, $stateParams, alertService){
		'ngInject';
		$reactive(this).attach($scope);
		this.alertService = alertService;
		this.$scope = $scope;

	}
	showSignIn() {
		this.$scope.$emit('showSignIn');
		// this.closeMenu()
	}
}

export default angular.module('login',[
	angularMeteor,
	uiRouter,
])
 .component('login', {
	templateUrl: 'imports/components/login.html',
	controller: ['$scope','$state','$rootScope','$reactive', '$stateParams', 'alertService', LoginCtrl]
 });