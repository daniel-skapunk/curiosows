import angular from 'angular';
import angularMeteor from 'angular-meteor';
import topbarTemplate from './topbar.html';

class topbarCtrl {
	constructor($scope, $rootScope, $state, $stateParams, alertService){
		$scope.viewModel(this);
		this.$state = $state;
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.alertService = alertService;
		this.showMenu = false;
		// make it dynamic?
		// this.menuItems = [{lbl:'Dashboard',val:'',showpublic:false}]
	}
	logout() {
		this.$scope.$emit('logout');
		this.closeMenu()
	}
	showSignIn() {
		this.$scope.$emit('showSignIn');
		this.closeMenu()
	}
	
	closeMenu(){
		this.showMenu=!this.showMenu;
	}
	gotoCallRoll(){
		var u = Meteor.user();
		var contactId =u.profile.contactId;
		this.closeMenu();
		this.$state.go('callroll');
	}
	canISee(role){ return this.$rootScope.canISee(role) }
}

export default angular.module('topbar',[
	angularMeteor
])
 .component('topbar', {
	templateUrl: 'imports/components/topbar.html',
	controller: ['$scope','$rootScope', '$state','alertService',topbarCtrl]
 });