import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Workshops } from '../api/workshops.js';
import { Contacts } from '../api/contacts.js';

import * as help from '../helpers.js';

import workshopsFormTemplate from './workshopsAddForm.html';
import workshopsAddTemplate from './workshopsAdd.html';
import workshopsEditTemplate from './workshopsEdit.html';

class WorkshopsAddFormCtrl {
	constructor($scope, $state, $rootScope){
		$scope.viewModel(this);
		//$reactive(this).attach($scope)
		this.subscribe('workshops');
		this.subscribe('contacts');

		this.$state = $state;
		this.$scope = $scope;
		this.weekDays = help.weekDays;
		console.debug(this.weekDays);
		this.selected = '';  // string to search teacher typeahead
		this.newWorkshop = {};

		this.helpers({
			contacts(){
				var str = this.getReactively('selected');
				//console.debug(this.getReactively('selected'));
				//return Contacts.find({first_name:str});
				//var contacts = Contacts.find({first_name: { $regex: str } })
				var contacts = Contacts.find({ $or: [
					{first_name: { $regex: str, $options: 'i' } },
					{last_name: { $regex: str, $options: 'i' } }
					]
				}); 
				//console.debug(contacts);
				return contacts;
				//Meteor.call('contacts.find', str);
			}
		});
	}

	typeaheadOnSelect($item, $model, $label, $event){
		this.newWorkshop.teacherId = $item._id;
	}
	addWorkshop(newWorkshop) {
		Meteor.call('workshops.insert', newWorkshop);
		this.newWorkshop = "";
		this.$state.go('workshops');
	}
	cancel(){
		this.$scope.$emit('goBack');
	}
	
}
class WorkshopsEditFormCtrl {
	constructor($scope, $reactive, $state, $rootScope, $stateParams){
		$scope.viewModel(this);
		$reactive(this).attach($scope)
		this.subscribe('workshops');
		this.subscribe('contacts');

		this.$state = $state;
		this.$stateParams = $stateParams;
		this.$scope = $scope;
		this.newWorkshop = {};
		this.weekDays = help.weekDays;
		this.$scope.workshopId = null;
		this.selected = '';  // string to search teacher typeahead

		console.debug($state);
		console.debug($stateParams);

		if($stateParams.workshopId){
			this.$scope.workshopId = this.$stateParams.workshopId;
		}

		this.autorun(() => {
			console.debug('autorun');
			this.newWorkshop = Workshops.findOne({_id:this.$scope.workshopId});
			if(this.newWorkshop){
				this.newWorkshop.teacher = Contacts.findOne({_id: this.newWorkshop.teacherId });
				//this.selected = this.newWorkshop.teacher.first_name + ' ' + this.newWorkshop.teacher.last_name;
			}
		});

		this.helpers({
			contacts(){
				var str = this.getReactively('selected');
				//console.debug(this.getReactively('selected'));
				//return Contacts.find({first_name:str});
				//var contacts = Contacts.find({first_name: { $regex: str } })
				var contacts = Contacts.find({ $or: [
					{first_name: { $regex: str, $options: 'i' } },
					{last_name: { $regex: str, $options: 'i' } }
					]
				}); 
				console.debug(contacts);
				return contacts;
				//Meteor.call('contacts.find', str);
			}
		})

		// watch when teacher info changes
		$scope.$watch('$ctrl.newWorkshop.teacher', function(newValue, oldValue){
			if(newValue){
				console.debug(newValue);
				$scope.$ctrl.selected = newValue.first_name + ' ' + newValue.last_name;
			}
		});
	}
	typeaheadOnSelect($item, $model, $label, $event){
		this.newWorkshop.teacherId = $item._id;
	}
	addWorkshop(newWorkshop) {
		Meteor.call('workshops.update', newWorkshop);
		this.newWorkshop = "";
		this.$state.go('workshops');
	}
	cancel(){
		console.debug('goback workshopsAddForm');
		this.$scope.$emit('goBack');
	}
}

export default angular.module('workshopsAddForm',[
	angularMeteor
])
 .component('workshopsAdd', {
	templateUrl: 'imports/components/workshopsAdd.html',
	//controller: WorkshopsAddFormCtrl
 })
 .component('workshopsEdit', {
	templateUrl: 'imports/components/workshopsEdit.html',
	//controller: WorkshopsAddFormCtrl
 })
 .component('workshopsAddForm', {
	templateUrl: 'imports/components/workshopsAddForm.html',
	controller: ['$scope', '$state', '$rootScope', WorkshopsAddFormCtrl]
 })
 .component('workshopsEditForm', {
	templateUrl: 'imports/components/workshopsAddForm.html',
	controller: ['$scope', '$reactive', '$state', '$rootScope', '$stateParams', WorkshopsEditFormCtrl]
 });