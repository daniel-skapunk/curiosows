import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Workshops } from '../api/workshops.js';
// import { Enrolments } from '../api/enrolments.js';
import template from './workshopsView.html';

import * as help from '../helpers.js';

class WorkshopsViewCtrl {
	constructor($scope, $reactive, $state, $stateParams, $rootScope){
		console.debug('WorkshopsViewCtrl');
		$scope.viewModel(this);
		$reactive(this).attach($scope);
		this.subscribe('workshops');
		this.$state = $state;
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.workshop = {};
		//this.enrolments = {};
		
		this.workshopId = $stateParams.workshopId;
		
		this.helpers({
			workshop(){
				console.debug('get workshop');
				console.debug(this.workshopId);
				var workshopId = this.getReactively('workshopId');
				var ws = Workshops.findOne({_id: workshopId});
				return ws;
			}
			// ,
			// enrolments(){
			// 	return Enrolments.find({workshop_id: workshopId})		
			// }
		})
	}
	
	daysToString(days){	return help.daysToString(days);}

	// addWorkshop(workshop) {
	// 	Meteor.call('workshops.update', workshop);
	// 	this.workshop = "";
	// 	this.$state.go('workshops');
	// }
}

export default angular.module('workshopsView',[
	angularMeteor
])
 .component('workshopsView', {
	templateUrl: 'imports/components/workshopsView.html',
	controller: ['$scope', '$reactive', '$state', '$stateParams','$rootScope', WorkshopsViewCtrl]
 });