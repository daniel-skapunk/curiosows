import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import { Contacts } from '../api/contacts.js';

import template from './agreement.html';
import * as help from '../helpers.js';

class AgreementCtrl {
	constructor($scope,$state, $rootScope, $reactive, $stateParams, alertService){
		this.alertService = alertService;
		this.$scope = $scope;
		'ngInject';
		$reactive(this).attach($scope);

	}

	addContact(contact){
		var response = grecaptcha.getResponse()
		Meteor.call('contacts.captchainsert', contact,response,
			help.getErrorFunction('mal!!!', this.alertService,
				this.$scope,
				()=>{},
				()=>{
					// this.newContact = "";
					// this.$state.go('contacts');
				}
			)
		);
		// console.log(response)
		// console.log('contact',contact);
		// this.$http({
		//   method: 'POST',
		//   data: {
		//   	secret:'6LfdvSAaAAAAAL_tfdbffMb8vzUAgQwyle-Q1Bm_',
		//   	response: response
		//   },
		//   url: 'https://www.google.com/recaptcha/api/siteverify'
		// }).then(function successCallback(response) {
		// 	console.log('CATPCHA OK')
		//     // this callback will be called asynchronously
		//     // when the response is available
		//   }, function errorCallback(response) {
		//   	console.log('CATPCHA NOT')
		//     // called asynchronously if an error occurs
		//     // or server returns response with an error status.
		//   });

	}

}

export default angular.module('agreement',[
	angularMeteor,
	uiRouter,
])
 .component('agreement', {
	templateUrl: 'imports/components/agreement.html',
	controller: ['$scope','$state','$rootScope','$reactive', '$stateParams', 'alertService', AgreementCtrl]
 });