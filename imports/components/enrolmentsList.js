import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Enrolments} from '../api/enrolments.js';
import { Workshops, WorkshopsHelpers } from '../api/workshops.js';
import { Contacts } from '../api/contacts.js';

import * as help from '../helpers.js';

import template from './enrolmentsList.html';

import moment from 'moment';
moment.locale("es");

class EnrolmentsListCtrl {
	constructor($scope, $reactive, $rootScope, alertService){
		$scope.viewModel(this);
		$reactive(this).attach($scope);
		// this.subscribe('enrolments');
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.alertService = alertService;
		
		this.enrolmentsPaymentsSum = 0;
		this.enrolmentsFeeSum = 0;

		this.today = moment().format('YYYY-MM-DD');
		
		this.months = help.months.slice();
		//this.months.unshift({lbl:'', val:'!'});
		this.months.unshift({lbl:''});
		
		this.years = help.years.slice();
		//console.debug(this.years);

		this.weekDays = help.weekDays;
		this.searchDefault = {
			month: new Date().getMonth()+1,
			year: new Date().getFullYear()
		}
		this.search = {
			month: new Date().getMonth()+1,
			year: new Date().getFullYear()
		};

		if(this.month)
			this.search.month = null;
		else if($rootScope.currentMonth)
			this.search.month = $rootScope.currentMonth;
		else
			$rootScope.currentMonth = this.search.month;
		//this.search2 = {month: 'Julio'};
		this.enrolmentsWeek = {};
		this.oneClassEnrolmentsWeek = {};
		var p = this;
		var scope = $scope;

		// Week Days in month
		// array of mondays, tuesdas, etc...
		// ex = [{weekDay:0, days:[7,14,21,28]},[...]]
		this.weekDaysInMonth = _.times(7, function(_index){
			//console.debug(p.search.year);
			var start = moment(p.search.year+'-'+p.search.month+'-01'); //var start = moment($scope).add(1, 'day');
			var daysInMonth = moment(p.search.year+'-'+p.search.month).daysInMonth();
			var end = moment(p.search.year+'-'+p.search.month+'-'+daysInMonth); //var end = moment(date).subtract(1, 'day'), weekdays, index
			
			var wd = help.weekDaysIn(start, end, _index+1)
			wd = _.map(wd, function(value){
				return moment(value).format('DD');
			});
			var obj = {weekDay: _index, days: wd };
			return obj;
		})

		
		
		//ORDER BY PARAMETERS
		this.reverse = false;
		if(this.orderBy=='month'){
			this.propertyName = ['year','month'];	
		} else this.propertyName = 'paymentsSum';
		
		$scope.$watch('$ctrl.search', function(newValue, oldValue){
			//console.debug(newValue);
			p.findEnrolmentsByWeek(newValue.month, newValue.year, $scope);
			$rootScope.currentMonth = newValue.month;
			p.findOneClassesByWeek(newValue.month, newValue.year, $scope)
		}, true);

		this.helpers({
			enrolments: function(){
				var enr;
				var month = this.getReactively( 'search.month' );
				var year = this.getReactively( 'search.year' );
				var contactId = this.getReactively( 'contactId' );
				var workshopId = this.getReactively( 'workshopId' );
				var srchObj = {};
				if(month){ srchObj.month = month; }
				if(year){ srchObj.year = year; }
				if(contactId){ srchObj.contactId = contactId; }
				if(workshopId){ srchObj.workshopId = workshopId; }
				//console.debug(srchObj);
				enr = Enrolments.find(srchObj);
				//console.debug(enr);
				return enr;
			},
		})

		this.autorun(() => {
			//console.debug('autorun');
			var month = this.getReactively( 'search.month' );
			var year = this.getReactively( 'search.year' );
			this.getReactively('workshopId');
			//console.debug(month);
			this.subscribe('enrolments', function(){ return [month, year]; } , {
				onStart: function(){
					$rootScope.loading = true;
				},
				onReady: function(){
					$rootScope.loading = false;
					var wsIds = this.enrolments.map(function(p) { return p.workshopId });
					var contactIds = this.enrolments.map(function(p) { return p.contactId });
					// console.debug('contactIds');
					// console.debug(contactIds);
					this.subscribe('enrolments_workshops', function(){ 
						return [wsIds, contactIds];
					},{
						onReady: function(){
							var sumPayments = 0;
							var sumFees = 0;
							this.enrolments.forEach(function(e){ 
								sumPayments += e.paymentsSum;
								sumFees += e.fee;

								e.workshop = Workshops.findOne({_id:e.workshopId});
								e.contact = Contacts.findOne({_id:e.contactId});
								if(e.workshop)
									contactIds.indexOf(e.workshop.teacherId) === -1 ? contactIds.push(e.workshop.teacherId):'';
							});
							this.enrolmentsPaymentsSum = sumPayments;
							this.enrolmentsFeeSum = sumFees;
							this.subscribe('enrolments_workshops', function(){ 
									//console.debug(contactIds);
									return [wsIds, contactIds];
								},{
									onReady: function(){
										this.enrolments.forEach(function(en){ 	
											if(en.workshop)
												en.workshop.teacher = Contacts.findOne({_id:en.workshop.teacherId});
										});

									}
							});
						}
					})
					if(!$scope.$$phase && !$rootScope.$$phase) {
						$scope.$apply();
					}
				}
			}); // subscribe workshops
		});
	}

	findEnrolmentsByWeek(month, year, scope){ 
		var parent = this;
		Meteor.call('enrolments.findByWeekDay2', month,year, false, function(err, data){
			parent.enrolmentsWeek = data;
			scope.$apply();
		});
	}

	findOneClassesByWeek(month, year, scope){ 
		//console.debug('findOneClassesByWeek');
		var parent = this;
		Meteor.call('enrolments.findByWeekDay', month,year, true, function(err, data){
			//angular.extend(parent.enrolmentsWeek, data);
			parent.oneClassEnrolmentsWeek = data;
			scope.$apply();
		});
	}

	deleteEnrolment(enrolment){
		//console.debug('deleteEnrolment');
		Meteor.call('enrolments.remove', enrolment);
	}

	showAddPayment(enrolment){
		enrolment.showAddPayment = true;
	};

	sendTicket(enrolment){
		// add string date to enrolment payments
		var enr={}; // enrolment local var
		angular.copy(enrolment, enr);
		_.map(enr.payments, function(payment){
			payment.date = moment(payment.paymentDate).format('DD MMM YYYY');
			return payment;
		});

		var subject = "Recibo " + this.getMonth(enrolment.month) + ' ' + enrolment.workshop.name;
		enr.monthString = this.getMonth(enrolment.month);
		enr.numberOfClasses = enrolment.attendances.length;

		this.call('enrolments.sendTicket',
			enrolment.contact.email,
			'daniel@curiosocirco.com',
			subject,
			enr,
			function(error){
				if(error){
					//console.debug('error'); console.debug(error);
					this.alertService.alert({message: error.reason, type:'danger'});
				}else{
					console.debug('success');
					this.alertService.alert({message: 'Recibo enviado!', type:'success'});
				}
				this.$scope.$apply();
		});
		
		// console.debug('sendTicket');
	};

	addPaymentEnrolment(enrolment, amount){
		enrolment.payments.push({ paymentDate: new Date(), amount:amount});
		Meteor.call('enrolments.addPayment', enrolment);	
	}
	payed(enrolment){
		//console.debug(enrolment);
		if(!enrolment.paymentsSum)
			return false;
		if(enrolment.paymentsSum < enrolment.fee)
			return false;
		else
			return true;
	}
	daysToString(days){
		return help.daysToString(days);
	}
	workshopToString(workshop){
		if(!workshop) return "sin taller";
		var r = WorkshopsHelpers.workshopToString(workshop);
		return r;
	}
	sortBy(propertyName) {
    	this.reverse = (propertyName !== null && this.equals(this.propertyName, propertyName) )? !this.reverse : false;
    	this.propertyName = propertyName;
    	//this.friends = orderBy(friends, this.propertyName, this.reverse);
  	};
  	equals(a,b){
  		return angular.equals(a,b);
  	};
  	getMonth(month){
  		if(month){
  			//console.debug(month);
  			return this.months[month].lbl;
  		}
  	};
  	filterBy(year_month){
  		if(!!year_month.year == false)
  			delete year_month.year;
  		//console.debug(year_month);
  		return year_month;
  	};
  	showAllChange(showAll){
  		console.debug(showAll);
  		if(showAll)
  			this.search ={};
  		else
  			this.search = this.searchDefault;
  	};
  	changeAttendance(enr,attendances, day){
  		//found =_.map([1, 2, 3], function(num){ return num * 3; });
  		//console.debug(day);
  		if(day.status==null || day.status==-1)
  			day.status = 1;
  		else if(day.status == 1)
  			day.status = 0;
  		else if(day.status == 0)
  			day.status = -1;
  		Meteor.call('enrolments.updateAttendances',enr.enrolmentId, day.day, day.status);
  	};
  	getDay(days,day){
  		// console.debug(day);
  		for (var i = 0; i < days.length; i++) {
  			d = days[i].day;
  			if(d==day) return days[i];
  		}
  		return false;
  	}
}

export default angular.module('enrolmentsList',[
	angularMeteor
])
 .component('enrolmentsList', {
	templateUrl: 'imports/components/enrolmentsList.html',
	controller: ['$scope', '$reactive', '$rootScope', 'alertService', EnrolmentsListCtrl],
	bindings: {
		orderBy: '@',
		month: '@',
    	contactId: '@',
    	workshopId: '='
  	}
 });



