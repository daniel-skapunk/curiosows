import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Contacts } from '../api/contacts.js';
import { Enrolments } from '../api/enrolments.js';
import template from './contactsView.html';

class ContactsViewCtrl {
	constructor($scope, $reactive, $state, $stateParams){
		$scope.viewModel(this);
		$reactive(this).attach($scope);
		this.subscribe('contacts')
		
		this.$state = $state;
		this.contact = {};
		this.enrolments = {};
		
		var contactId = $stateParams.contactId
		
		this.helpers({
			contact(){
				return Contacts.findOne({_id: contactId});
			},
			enrolments(){
				return Enrolments.find({contact_id: contactId})		
			}
		})
	}
	addContact(contact) {
		Meteor.call('contacts.update', contact);
		this.contact = "";
		this.$state.go('contacts');
	}
}

export default angular.module('contactsView',[
	angularMeteor
])
 .component('contactsView', {
	templateUrl: 'imports/components/contactsView.html',
	controller: ['$scope', '$reactive', '$state', '$stateParams', ContactsViewCtrl]
 });