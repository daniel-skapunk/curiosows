import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Enrolments } from '../api/enrolments.js';
import { Workshops, WorkshopsHelpers  } from '../api/workshops.js';
import { Contacts } from '../api/contacts.js';

import * as help from '../helpers.js';

import template from './enrolmentsAddForm.html';
import template2 from './enrolmentsAdd.html';
import template3 from './enrolmentsEdit.html';

class EnrolmentsAddEditFormCtrl {
	constructor($scope, $rootScope, $reactive, $state, $stateParams, alertService){
		$scope.viewModel(this);
		$reactive(this).attach($scope);
		this.subscribe('workshops');
		this.subscribe('contacts');


		this.alertService = alertService;

		this.$state = $state;
		this.$stateParams = $stateParams;
		this.$scope = $scope;
		this.selected = '';
		this.model = {};
		this.calendarSelectedDates = []; // moment() array of the classes dates
		this.months = help.months;
		this.weekDays = help.weekDays;
		this.years = help.years.slice();
		this.fee = help.fee;

		this.options = {
			//customClass: getDayClass,
			//minDate: new Date(),
			showWeeks: true
		};

		var parent = this;

		this.monthDatePicker = moment();
		
		this.newEnrolment = {
			contactId: '',
			paymentsSum: 0,
			oneClass: false,
			oneClassDate:null,
			month: new Date().getMonth()+1,
			year: new Date().getFullYear(),
			fee: this.fee,
			payments:[],
			days:[] 	// the week days the workshop is taken ex Mon, Wed: [0,2] 
		};

		// ==============================
		// WATCHERS / LISTENERS
		// ==============================
		
		/**
		 * workhopId Watcher, listen if selected workshop changes in view
		 * @param  {[Number]} newValue  new workshopId
		 * @param  {[Number]} oldValue  old workshopId
		 * @return nothing
		 */
		$scope.$watch('$ctrl.newEnrolment.workshopId', function(newValue, oldValue){
			//console.debug('$ctrl.newEnrolment.workshopId');
			// If newEnrolment null return
			if(!$scope.$ctrl.newEnrolment || !newValue) return;
			console.debug($scope.$ctrl.newEnrolment);
			if(!oldValue && $stateParams.enrolmentId)
				return; //Do nothing, REST data is changind the model, dont have tu update calendar
			
			// Get and set workshop in the scope
			var data = _.find($scope.$ctrl.workshops, function(ws){ return ws._id == newValue; });
			$scope.$ctrl.newEnrolment.workshop = data;
			$scope.$ctrl.newEnrolment.days = data.days;

			//UPDATE CALENDAR SELECTED DATES
			
			var attArr = help.getAttendanceArray($scope.$ctrl.newEnrolment.month,
				$scope.$ctrl.newEnrolment.year, $scope.$ctrl.newEnrolment.days);
			$scope.$ctrl.calendarSelectedDates = help.attArrToCalendar(
				$scope.$ctrl.newEnrolment.month,
				$scope.$ctrl.newEnrolment.year, attArr);
			console.log('$scope.$ctrl.calendarSelectedDates: ',$scope.$ctrl.calendarSelectedDates );
			
		}, true);

		$scope.$watch('$ctrl.newEnrolment.oneClassDate', function(newOneClassDate, oldValue){
			if(newOneClassDate){
				
				var day = (newOneClassDate.getDay() || 7)-1;
				$scope.$ctrl.newEnrolment.days = [day];


				var d = moment(newOneClassDate);
				$scope.$ctrl.calendarSelectedDates = [];
				$scope.$ctrl.calendarSelectedDates.push(d);
			} else{
				if(!$scope.$ctrl.newEnrolment.workshop)
					return;
				// UPDATE DAYS
				$scope.$ctrl.newEnrolment.days = angular.copy($scope.$ctrl.newEnrolment.workshop.days);
				
				//UPDATE CALENDAR SELECTED DATES
				attArr = help.getAttendanceArray($scope.$ctrl.newEnrolment.month,
					$scope.$ctrl.newEnrolment.year, $scope.$ctrl.newEnrolment.workshop.days);
				$scope.$ctrl.calendarSelectedDates = help.attArrToCalendar(
					$scope.$ctrl.newEnrolment.month,
					$scope.$ctrl.newEnrolment.year, attArr);
			}

		}, true);

		$scope.$watch('$ctrl.calendarSelectedDates', function(newSelDates, oldValue){
			//console.debug('calendarSelectedDates change!');
			if($scope.$ctrl.newEnrolment.attendances==newSelDates) return
			
			$scope.$ctrl.newEnrolment.attendances = help.calendarToAttArr(newSelDates, $scope.$ctrl.newEnrolment.attendances)
			//console.debug($scope.$ctrl.newEnrolment.attendances);
		}, true);


		$scope.$watch('$ctrl.newEnrolment', function(newValue, oldValue){
			if(newValue){
				if(newValue.contact){
					parent.selected = Contacts.getFullName(newValue.contact);
				}
			}
		}, true);

		/// END OF WATCHERS SECTION
		

		// ContactId Param present 
		if($stateParams.contactId){
			var test = Meteor.call('contacts.find', $stateParams.contactId , function(error, result){
				// if error log to debug
				if(error) console.debug(error);
				// update new enrolment with the fetched contact
				parent.newEnrolment.contact = result;
				parent.newEnrolment.contactId = result._id;
				$scope.$digest();
				// update text input
				//parent.selected = Contacts.getFullName(result);
				//parent.$scope.$apply();
			});
		}
		
		// EnrolmentId Param present: User wants to Edit enrolment
		if($stateParams.enrolmentId){
			//console.debug('$stateParams.enrolmentId');
			Meteor.call('enrolments.find', $stateParams.enrolmentId, function(error, result){
				// if error log to debug
				if(error) console.debug(error);
				var e = result;

				parent.calendarSelectedDates = help.attArrToCalendar(result.month,
					parent.newEnrolment.year, e.attendances);
				console.log('parent.calendarSelectedDates: ',parent.calendarSelectedDates );
				angular.extend(parent.newEnrolment, e);

				parent.monthDatePicker.year(parent.newEnrolment.year).month(parent.newEnrolment.month-1);
				console.log(parent.monthDatePicker);
				$scope.$digest();
			});
			

	} // END OF CONSTRUCTOR

		//console.debug('before help');
		this.helpers({
			workshops: function(){
				return Workshops.find({archived:{ $ne: true } });
			},
			contacts(){
				var str = this.getReactively('selected');
				//console.debug(this.getReactively('selected'));
				//return Contacts.find({first_name:str});
				//var contacts = Contacts.find({first_name: { $regex: str } })
				var contacts = Contacts.find({ $or: [
					{first_name: { $regex: str, $options: 'i' } },
					{last_name: { $regex: str, $options: 'i' } }
					]
				}); 
				//console.debug(contacts);
				return contacts;
				//Meteor.call('contacts.find', str);
			}
		})
	}
	typeaheadOnSelect($item, $model, $label, $event){
		this.newEnrolment.contactId = $item._id;
	}
	addEnrolment(newEnrolment) {
		console.debug('addEnrolment');
		if(newEnrolment.payment){
			this.newEnrolment.payments = [{
				paymentDate: new Date(), 
				amount: newEnrolment.payment
			}];
			delete newEnrolment.payment;
		}
		//console.debug(this.newEnrolment);
		var successMessage = newEnrolment._id ? "Inscripción Actualizada" :  "Inscripción Agregada";
		delete newEnrolment.ticketSentStatus;
		this.call('enrolments.insertupdate', newEnrolment, 
			help.getErrorFunction(successMessage, this.alertService, this.$scope,
				()=>{},
				()=>{
					this.newEnrolment = "";
					this.$state.go('enrolments');
				}
			)
		);
	}
	deletePayment(payment){
		console.debug('deletePayment');
		var index = this.newEnrolment.payments.indexOf(payment);
		this.newEnrolment.payments.splice(index,1)
		//enrolment.payments.({ paymentDate: new Date(), amount:amount});
		//Meteor.call('enrolments.addPayment', this.newEnrolment);	
	}
	addPayment(newPaymentAmount){
		console.debug('addPayment');
		this.newEnrolment.payments.push({ paymentDate: new Date(), amount:newPaymentAmount});
		this.newPaymentAmount='';
		//enrolment.payments.({ paymentDate: new Date(), amount:amount});
		//Meteor.call('enrolments.addPayment', this.newEnrolment);	
	}
	cancel(){
		this.$scope.$emit('goBack');
	}
	
	workshopToString(workshop){
		var r = WorkshopsHelpers.workshopToString(workshop);
		return r;
	}

	oneClassChange(){
		// IF OneClass Checkbox true: set OneClasDate value to newEnrolment
		if(this.newEnrolment.oneClass){	this.newEnrolment.oneClassDate = new Date(); }
		else{ this.newEnrolment.oneClassDate = null; }
	}

	keydownHandler(event){
		console.debug(event);
		if(event.keyCode == 13){
			console.debug('enter keyCode');
			this.addPayment(this.newPaymentAmount);
			event.preventDefault(); event.stopPropagation();
		}
	}
}

export default angular.module('enrolmentsAddForm',[
	angularMeteor
])
 .component('enrolmentsAdd', {
	templateUrl: 'imports/components/enrolmentsAdd.html',
	//controller: EnrolmentsAddFormCtrl
 })
 .component('enrolmentsEdit', {
	templateUrl: 'imports/components/enrolmentsEdit.html',
	//controller: EnrolmentsAddCtrl
 })
 .component('enrolmentsEditForm', {
	templateUrl: 'imports/components/enrolmentsAddForm.html',
	controller: ['$scope', '$rootScope', '$reactive', '$state', '$stateParams', 'alertService', EnrolmentsAddEditFormCtrl]
 })
 .component('enrolmentsAddForm', {
	templateUrl: 'imports/components/enrolmentsAddForm.html',
	controller: ['$scope', '$rootScope', '$reactive', '$state', '$stateParams', 'alertService', EnrolmentsAddEditFormCtrl]
 });