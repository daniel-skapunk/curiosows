import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Workshops } from '../api/workshops.js';

import * as help from '../helpers.js';

import template from './workshopsList.html'; 

class WorkshopsListCtrl {
	constructor($scope, $reactive, $rootScope){
		$reactive(this).attach($scope);
		this.subscribe('workshops');
		console.log('test: ');
		
		$scope.viewModel(this);

		var thi = this;
		this.weekDays = help.weekDays;

		this.helpers({
			workshops: function(){
				return Workshops.find({archived:{ $ne: true } });
			},
			archived: function(){
				return Workshops.find({archived:true });	
			}
		});

		// this.autorun(() => {
		// 	//console.debug('subscribe workshops');
		// 	this.subscribe('workshops'); // subscribe workshops
		// });
	}

	archiveWorkshop(workshop){
		console.debug('delete');
		Meteor.call('workshops.softDelete', workshop);
	}
	deleteWorkshop(workshop){
		console.debug('delete');
		Meteor.call('workshops.remove', workshop);
	}
	restoreWorkshop(workshop){
		console.debug('delete');
		Meteor.call('workshops.restore', workshop);
	}
	dayToString(day){
		var d = help.dayToString(day);
		return d;
	}
}

export default angular.module('workshopsList',[
	angularMeteor,
	uiRouter
])
 .component('workshopsList', {
	templateUrl: 'imports/components/workshopsList.html',
	controller: ['$scope', '$reactive', '$rootScope', WorkshopsListCtrl]
 });