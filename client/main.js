import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import modal from 'angular-ui-bootstrap/src/modal';
import checklistModel from 'checklist-model';

import loginModal from '../imports/components/loginModal.html';
import {loginModalCtrl} from '../imports/components/loginModal.js';
import mainCtrl from './mainCtrl.js';

import { Router } from 'meteor/iron:router';

// IMPORT JSs WITH CONTROLLERS AND MODULE DEFINITIONS
import dashboardView from '../imports/components/dashboardView';
import agreement from '../imports/components/agreement';
import topbar from '../imports/components/topbar';
import login from '../imports/components/login';

import contactsList from '../imports/components/contactsList';
import contactsAddForm from '../imports/components/contactsAddForm';
import contactsView from '../imports/components/contactsView'; 

import workshopsList from '../imports/components/workshopsList';
import workshopsAddForm from '../imports/components/workshopsAddForm';
import workshopsView from '../imports/components/workshopsView';

import usersAddForm from '../imports/components/usersAddForm';

import enrolmentsList from '../imports/components/enrolmentsList';
import enrolmentsWeekMonthly from '../imports/components/enrolmentsWeekMonthly';
import enrolmentsAddForm from '../imports/components/enrolmentsAddForm';

import callroll from '../imports/components/callroll';
import callrollEnrolmentsList from '../imports/components/callrollEnrolmentsList';

// LOAD ANGULAR SERVICES TO SHARE BETWEEN CONTROLLERS
import alertService from '../imports/services/alertService';

// LOAD OTHER EXTERNAL LIBRARIES
import moment from 'moment';
import locale_es from "moment/locale/es";
moment.locale('es');
_ = lodash;
import 'gm.datepicker-multi-select/src/gm.datepickerMultiSelect.js'
import 'multiple-date-picker/dist/multipleDatePicker.js'
import 'multiple-date-picker/dist/multipleDatePicker.css'

// LOAD METEOR THINGS
import { Meteor } from 'meteor/meteor';
// meteor users account configuration
import '../imports/startup/accounts-config.js';  

// Load angular Modules
angular.module('curiosows', [
  angularMeteor,
  uiRouter,
  uiBootstrap,
  'gm.datepickerMultiSelect',
  //mdp,
  'multipleDatePicker',
  checklistModel,
  'alertModule',

  dashboardView.name,
  agreement.name,
  login.name,
  topbar.name,
  callroll.name,
  callrollEnrolmentsList.name, 
  
  contactsList.name,
  contactsAddForm.name,
  contactsView.name,
  
  workshopsList.name,
  workshopsAddForm.name,
  workshopsView.name,

  usersAddForm.name,
  
  enrolmentsList.name,
  enrolmentsAddForm.name,
  enrolmentsWeekMonthly.name,
  'accounts.ui'
])
	.config(['$locationProvider', '$urlRouterProvider', '$stateProvider',config]);

function config($locationProvider, $urlRouterProvider, $stateProvider) {
  'ngInject';
 
  $locationProvider.html5Mode(true);
 
  $urlRouterProvider.otherwise('/contacts');

  $stateProvider
  	.state('dashboard', {
		url: '/dashboard' ,
		template: '<dashboard></dashboard>'
	})

	.state('agreement', {
		url: '/agreement' ,
		template: '<agreement></agreement>'
	})
	.state('login', {
		url: '/login' ,
		template: '<login></login>'
	})
	// USERS
	.state('usersAddForm', {
		url: '/users/add' ,
		template: '<users-add-form></users-add-form>'
	})

	// CONTACTS
	.state('contacts', {
		url: '/contacts' ,
		template: '<contacts-list></contacts-list>'
	})
	.state('contactsAdd', {
		url: '/contacts/add' ,
		template: '<contacts-add></contacts-add>'
	})
	.state('contactsView', {
		url: '/contacts/view/:contactId' ,
		template: '<contacts-view></contacts-view>'
	})
	.state('contactsEdit', {
		url: '/contacts/edit/:contactId' ,
		template: '<contacts-edit></contacts-edit>'
	})
	
	// WORKSHOPS
	.state('workshops', {
		url: '/workshops' ,
		template: '<workshops-list></workshops-list>'
	})
	.state('workshopsAdd', {
		url: '/workshops/add' ,
		template: '<workshops-add></workshops-add>'
	})
	.state('workshopsView', {
		url: '/workshops/view/:workshopId' ,
		template: '<workshops-view></workshops-view>'
	})
	.state('workshopsEdit', {
		url: '/workshops/edit/:workshopId' ,
		template: '<workshops-edit></workshops-edit>'
	})

	// PAYMENTS
	.state('payments', {
		url: '/payments' ,
		template: '<payments-list></payments-list>'
	})
	.state('paymentsAddForm', {
		url: '/payments/add' ,
		template: '<payments-add-form></payments-add-form>'
	})

	// ENROLMENTS
	.state('enrolmentsAddForm', {
		url: '/enrolments/add/:contactId' ,
		template: '<enrolments-add></enrolments-add>'
	})
	.state('enrolmentsEdit', {
		url: '/enrolments/edit/:enrolmentId' ,
		template: '<enrolments-edit></enrolments-edit>'
	})
	.state('callroll', {
		url: '/callroll' ,
		template: '<callroll></callroll>'
	})
	.state('enrolments', {
		url: '/enrolments' ,
		template: '<enrolments-list></enrolments-list>'
	});


}

angular.module('curiosows').controller('mainCtrl', [
	'$rootScope', '$scope', '$reactive',
	'$state', '$uibModal', 'alertService',mainCtrl]);

angular.module('curiosows').controller('ModalInstanceCtrl', [
	'$uibModalInstance', '$state', 'alertService',loginModalCtrl]);

Router.configure({
  layoutTemplate: 'noRoutesTemplate',
  template: 'noRoutesTemplate',
  noRoutesTemplate: 'noRoutesTemplate'      
});

