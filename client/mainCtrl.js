// LOAD OUR LIBRARIES
import * as help from '../imports/helpers.js';
_ = lodash;

export default function ($rootScope, $scope, $reactive,
	$state, $uibModal, alertService){

  	$reactive(this).attach($scope);
  	this.$state = $state;

	$rootScope.range = function(start, count) {
	  return Array.apply(0, Array(count))
		.map(function (element, index) { 
		  return index + start;  
	  });
	};

	$rootScope.canISee = function (role) {
		role = role ? role : 'teachers';
		var u = Meteor.user();
		var r = Roles.userIsInRole ( u, role);
		return r;  
	}

	$scope.gotoCallRoll = function(){
		// var u = Meteor.user();
		// var contactId =u.profile.contactId;
		// console.log('contactId: ', contactId); 
		$state.go('callroll');
	}


	$scope.appModel = $rootScope;
	$scope.appModel.alerts = alertService.alerts;
	$rootScope.back = '';
	$rootScope.months = help.months.slice();

	$rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
		//assign the "from" parameter to something
		$rootScope.back = from;
		$rootScope.backParams = fromParams;
		// console.debug(ev);
		// console.debug(from);
		// console.debug(fromParams);
		// str.replace(from.name)
	});

	$scope.closeAlert = function(index) {
		alertService.closeAlert(index);
	};

	$scope.open = function (size, parentSelector) {
		var parentElem = parentSelector ? 
		  angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
		  animation: true,
		  ariaLabelledBy: 'modal-title',
		  ariaDescribedBy: 'modal-body',
		  templateUrl: 'imports/components/loginModal.html',
		  controller: 'ModalInstanceCtrl',
		  controllerAs: '$ctrl',
		  bindToController: true,
		  size: size,
		  appendTo: parentElem,
		 //  resolve: {
			// items: function () {
			//   return ['item1', 'item2', 'item3'];
			// }
		 //  }
		});

		modalInstance.result.then(function (result) {
			//
			
		}, function () {
		  console.log('Modal dismissed at: ' + new Date());
		});
	};

	// $scope.login = function(loginData){
	// 	console.debug(loginData);
	// 	console.debug('login');
	// 	Meteor.loginWithPassword($scope.model.loginData.email, $scope.model.loginData.password, function(err){
	// 		console.log('currentUser', currentUser)
	// 		if(!err)
	// 			console.debug('loggeado');
	// 		else
	// 			console.debug(err);
	// 	});
	// };

	$scope.logout = function(){
		console.log('logout: ');
		Meteor.logout(function(err){
			if(err)
				console.debug(err);
			else
				console.debug('logged-out');
		});
	}
	$scope.$on('logout',function(){
		$scope.logout();
	})
	$scope.$on('showSignIn',function(){
		console.log('open sign in');
		$scope.open('sm')
	})
	
	$scope.$on('goBack',function(){
		try{
			$state.go($rootScope.back.name, $rootScope.backParams);
		} catch (error){
			console.debug(error);
		}
	})

	// Catch evento when STATE NOT FOUND
	$rootScope.$on('$stateNotFound', 
	function(event, unfoundState, fromState, fromParams){ 
		$state.go('callroll');
	})


	this.helpers({
		test: ()=>{
			var user = this.getReactively('$rootScope.currentUser')
			var state = this.getReactively('$state.current.name')
			var userId =Meteor.userId();
			var auth = $rootScope.canISee('admin')
			// console.log(state)
			if( !userId && !auth ){
				if(state){
					// chechk if wants not auth resource
					var r = help.routeClean(state)
					console.log('cant see!', state, r)
					// console.log('state', state) 
					// console.log(r)
					if(!r)
						$state.go('login')
				}
			}
			else if(userId && !auth){
				console.log('waiting for User confirmation')
			}else{
				console.log('User is authenticated!')
			}
  	// 		console.log('Meteor.userId', Meteor.userId())
			// console.log('$rootScope.currentUser',$rootScope.currentUser)
			// console.log('canISee', $rootScope.canISee('admin'))
			// if(!$rootScope.canISee('admin')){
			// 	$state.go('enrolments');
			// }
		}
	})
}